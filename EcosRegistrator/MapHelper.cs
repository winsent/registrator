﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Controls.Maps;
using Windows.Storage.Streams;

namespace EcosRegistrator
{
    class MapHelper
    {
        static Geolocator geoLocation { get; set; }

        static void InitGeolocation()
        {
            if (geoLocation == null)
                geoLocation = new Geolocator() 
                    { 
                        DesiredAccuracy = PositionAccuracy.High, 
                        MovementThreshold = 2 
                    };
        }

        public static async void SetMapToMyCurrentPosition(MapControl map)
        {
            InitGeolocation();

            var basicGeoposition = await GetCurrenPosition();

            var mapIcon = AddMyLocationIcon(map, basicGeoposition);

            await map.TrySetViewAsync(mapIcon.Location, 14, 0, 0, MapAnimationKind.Bow);
        }

        public static async void SetMapByCoordinates(MapControl map, double[] coordinates)
        {
            var geopoint = new Geopoint(new BasicGeoposition
            {
                Latitude = coordinates[1],
                Longitude = coordinates[0]
            });

            await map.TrySetViewAsync(geopoint, 14);
        }

        public static async Task<BasicGeoposition> GetCurrenPosition()
        {
            InitGeolocation();

            var currentPostion = await geoLocation.GetGeopositionAsync();

            var basicGeoposition = new BasicGeoposition();
            basicGeoposition.Latitude = currentPostion.Coordinate.Point.Position.Latitude;
            basicGeoposition.Longitude = currentPostion.Coordinate.Point.Position.Longitude;

            return basicGeoposition;
        }

        static MapIcon CreateMapIcon(BasicGeoposition position, RandomAccessStreamReference image, string title)
        {
            var mapIcon = new MapIcon();
            mapIcon.Image = image;
            mapIcon.Title = title;
            mapIcon.NormalizedAnchorPoint = new Windows.Foundation.Point(0.5, 0.5);
            
            mapIcon.Location = new Geopoint(new BasicGeoposition
            {
                Latitude = position.Latitude,
                Longitude = position.Longitude
            });

            return mapIcon;
        }

        public static MapIcon AddObjectPoint(MapControl map, double[] position)
        {
            var basicGeoposition = new BasicGeoposition();
            basicGeoposition.Latitude = position[1];
            basicGeoposition.Longitude = position[0];

            var image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/ObjectPosition.png"));

            var mapIcon = CreateMapIcon(basicGeoposition, image, "Объект");

            map.MapElements.Add(mapIcon);
            
            return mapIcon;
        }

        public static MapIcon AddMyLocationIcon(MapControl map, double[] position)
        {
            var basicGeoposition = new BasicGeoposition();
            basicGeoposition.Latitude = position[1];
            basicGeoposition.Longitude = position[0];

            var image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/myLocation.png"));

            var mapIcon = CreateMapIcon(basicGeoposition, image, "Вы здесь");

            map.MapElements.Add(mapIcon);

            return mapIcon;
        }

        public static MapIcon AddMyLocationIcon(MapControl map, BasicGeoposition position)
        {
            var basicGeoposition = position;

            var image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/myLocation.png"));

            var mapIcon = CreateMapIcon(basicGeoposition, image, "Вы здесь");

            map.MapElements.Add(mapIcon);

            return mapIcon;
        }

        public static Geolocator GetGeolocator()
        {
            InitGeolocation();

            return geoLocation;
        }

        public static MapPolyline InitMapLine(List<BasicGeoposition> listBasicGeoPosition)
        {
            var path = new Geopath(listBasicGeoPosition);

            var line = new MapPolyline();
            line.StrokeColor = Windows.UI.Color.FromArgb(255, 220, 29, 29);
            line.StrokeThickness = 5;
            line.Path = path;

            return line;
        }
    }
}
