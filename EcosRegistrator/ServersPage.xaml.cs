﻿using EcosRegistrator.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Основная страница" см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace EcosRegistrator
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class ServersPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        List<RegistratorServer> serversList { get; set; }

        public ServersPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            FillListView();
            SetPageEvents();
        }

        async void FillListView()
        {
            ProgressRingLoad.Visibility = Windows.UI.Xaml.Visibility.Visible;

            serversList = await ServersData.GetList();

            if (serversList == null)
                serversList = new List<RegistratorServer>();

            foreach (var server in serversList)
            {
                CreateListViewItem(server);
            }

            ProgressRingLoad.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        void CreateListViewItem(RegistratorServer server)
        {
            var grid = new Grid();
            grid.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(125,0,180,170));
            grid.Margin = new Thickness(5);

            var secondColumn = new ColumnDefinition();
            secondColumn.Width = new GridLength(60);

            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(secondColumn);

            var textBlockName = new TextBlock();
            textBlockName.Text = server.Name;
            textBlockName.FontSize = 20;
            textBlockName.Margin = new Thickness(5);

            var textBlockAdress = new TextBlock()
            {
                Text = server.Adress,
                FontSize = 16,
                VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top,
                Margin = new Thickness(5, 35, 5, 5),
                TextWrapping = TextWrapping.Wrap
            };

            var textBlockDelete = new TextBlock();
            textBlockDelete.FontSize = 34;
            textBlockDelete.FontFamily = new FontFamily("Segoe UI Symbol");
            textBlockDelete.Margin = new Thickness(5);
            textBlockDelete.TextAlignment = TextAlignment.Center;
            textBlockDelete.Text = "\uE107";
            textBlockDelete.SetValue(Grid.ColumnProperty, 1);

            textBlockDelete.Tapped += async (sender, args) =>
            {
                if (await ServersData.DeleteServer(server))
                    ListViewServers.Items.Remove(grid);
                else
                    Debug.WriteLine("сервер не удален");

                ListViewServers.Focus(FocusState.Programmatic);
            };

            grid.Children.Add(textBlockName);
            grid.Children.Add(textBlockAdress);
            grid.Children.Add(textBlockDelete);

            ListViewServers.Items.Add(grid);
        }

        void SetPageEvents()
        {
            ButtonAddServer.Click += async (sender, args) =>
            {
                try
                {
                    if (TextBoxServerName.Text == String.Empty)
                        throw new Exception("Имя сервера обязательно для заполнения");

                    var serverAdress = TextBoxServerAdress.Text;

                    if (serverAdress == String.Empty)
                        throw new Exception("Адрес сервера обязательно для заполнения");

                    if (serverAdress.EndsWith("/"))
                        serverAdress = serverAdress.Remove(serverAdress.Length -1);

                    var httpString = "http://";

                    if (serverAdress.Substring(0, 7) != httpString)
                        serverAdress = String.Concat(httpString, serverAdress);

                    var server = new RegistratorServer(TextBoxServerName.Text, serverAdress);

                    if (await ServersData.AddServer(server))
                        CreateListViewItem(server);
                }
                catch (Exception exception)
                {
                    Debug.WriteLine("Сервер не добавлен. " + exception.Message);
#pragma warning disable
                    App.ShowMessageDialog("Сервер не добавлен. " + exception.Message);
#pragma warning restore
                }
            };
        }

        /// <summary>
        /// Получает объект <see cref="NavigationHelper"/>, связанный с данным объектом <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Получает модель представлений для данного объекта <see cref="Page"/>.
        /// Эту настройку можно изменить на модель строго типизированных представлений.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Сохраняет состояние, связанное с данной страницей, в случае приостановки приложения или
        /// удаления страницы из кэша навигации.  Значения должны соответствовать требованиям сериализации
        /// <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">Источник события; как правило, <see cref="NavigationHelper"/></param>
        /// <param name="e">Данные события, которые предоставляют пустой словарь для заполнения
        /// сериализуемым состоянием.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region Регистрация NavigationHelper

        /// <summary>
        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// <para>
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="NavigationHelper.LoadState"/>
        /// и <see cref="NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.
        /// </para>
        /// </summary>
        /// <param name="e">Предоставляет данные для методов навигации и обработчики
        /// событий, которые не могут отменить запрос навигации.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
