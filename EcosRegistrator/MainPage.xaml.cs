﻿#region Namespaces
using EcosRegistrator.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
#endregion
// Документацию по шаблону элемента "Основная страница" см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace EcosRegistrator
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        NavigationHelper navigationHelper;
        ObservableDictionary defaultViewModel = new ObservableDictionary();
        
        ObjectsModel objectsSchemeList {get; set;}
        List<TypedObjectData> typedObjectDataList { get; set; }
        List<GenericObjectTypedList> typedGenericObjectList { get; set; }
        List<DraftPost> draftPostsList { get; set; }
        string selectedModelResource { get; set; }

        const string namePropertyString = "name";
        const string createdPropertyString = "created";

        public static Style textBoxTransparentStyle { get; set; }

        public MainPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            textBoxTransparentStyle = AppMainPage.Resources["TransparentStyle"] as Style;

            if (ApplicationData.Current.LocalSettings.Values[App.filterType] != null)
                selectedModelResource = ApplicationData.Current.LocalSettings.Values[App.filterType].ToString();
            else
                selectedModelResource = ComboBoxItemAllObjects.Name;

            PopulateControls();
            SetPageEvents();
            GenericObject.DeleteWorkingGenericObject();
            ObjectModel.DeleteWorkingObjectModel();
        }

        void SetPageEvents()
        {
            ImageSettings.Tapped += async (sender, e) =>
            {
                //?ToDo: popup menu with settings.
                await App.ShowMessageDialog("все настройки, внизу будут смотреться лучше, по феншую пользователя windows");
            };

            AppBarButtonAdd.Click += (sender, e) =>
            {
                ListViewObjects.IsEnabled = false;
                FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout((FrameworkElement)sender);
                flyoutBase.ShowAt((FrameworkElement)AppBarButtonAdd);
            };

            AppBarButtonSinc.Click += async (sender, e) =>
            {
                try
                {
                    if (typedObjectDataList == null)
                        return;

                    if (progressRing.IsActive)
                        throw new Exception("Попробуйте позже. Выполняется другое действие.");

                    StartProgressRing();
                    MainPageCommandBar.IsEnabled = false;
                    ComboBoxType.IsEnabled = false;
                    ListViewObjects.IsItemClickEnabled = false;
                    Debug.WriteLine("Starting synchronisation!...");

                    await UpdateEditedPosts();
                    await PostDrafts();
                    await UpdateObjectsDataFromNet();


                    ListViewObjects.Items.Clear();

                    typedObjectDataList = null;
                    typedGenericObjectList = null;

                    typedObjectDataList = await TypedObjectData.GenerateList();
                    typedGenericObjectList = GenericObjectTypedList.GenerateList(typedObjectDataList);
                    
                    PopulateListView();
                }
                catch(Exception exception)
                {
                    Debug.WriteLine("Synchronisation failed. Error: " + exception.Message);
#pragma warning disable
                    App.ShowMessageDialog("Невозможно выполнить операцию. " + exception.Message);
#pragma warning restore
                }

                StopProgressRing();
                MainPageCommandBar.IsEnabled = true;
                ComboBoxType.IsEnabled = true;
                ListViewObjects.IsItemClickEnabled = true;
                
                Debug.WriteLine("Synchronisation ends.");
                await App.ShowMessageDialog("Данные синхронизированны!");
            };

            AppBarButtonClearData.Click += (sender, e) =>
            {
                StartProgressRing();

                //deleting storage files
                TablesData.DeleteObjectsScheme();
                DraftPostsHelper.DeleteDraftPostsDataFile();

                if (objectsSchemeList != null && objectsSchemeList.objects != null)
                {
                    foreach (var objectModel in objectsSchemeList.objects)
                    {
                        TypedObjectData.DeleteObjectsData(objectModel);
                    }
                }
                
                objectsSchemeList = null;
                typedObjectDataList = null;
                typedGenericObjectList = null;

                //deleting cookie
                var bpf = new Windows.Web.Http.Filters.HttpBaseProtocolFilter();
                var cookies = bpf.CookieManager.GetCookies(new Uri(App.baseUrl));

                foreach (var cookie in cookies)
                {
                    bpf.CookieManager.DeleteCookie(cookie);
                }

                StopProgressRing();

                Frame.Navigate(typeof(AuthorizePage));
            };

            AppBarButtonLogOut.Click += (sender, args) =>
            {
                Application.Current.Exit();
            };

            ListViewObjects.ItemClick += (sender, e) =>
            {
                var grid = (Grid)e.ClickedItem;

                if (grid.Tag.GetType() == typeof(GenericObject))
                {
                    var genericObject = (GenericObject)grid.Tag;
                    Frame.Navigate(typeof(ShowObjectPage), genericObject);
                }
                else 
                {
                    var draftPost = (DraftPost)grid.Tag;
                    Frame.Navigate(typeof(CreateObjectPage), draftPost);
                }
            };

            AddObjectMenuFlyout.Closed += (sender, args) =>
            {
                ListViewObjects.IsEnabled = true;
            };
        }

        async void PopulateControls()
        {
            try
            {
                StartProgressRing();

                draftPostsList = await DraftPostsHelper.GetList();
                objectsSchemeList = TablesData.objectsScheme ?? await TablesData.GetObjectsScheme();

                if (objectsSchemeList == null)
                {
                    TablesData.DeleteObjectsScheme();
                    Frame.Navigate(typeof(AuthorizePage));
                    throw new Exception("PopulateControls method. objectsSchemeList == null");
                }

                if (typedObjectDataList == null)
                    typedObjectDataList = await TypedObjectData.GenerateList();

                if (typedGenericObjectList == null)
                    typedGenericObjectList = GenericObjectTypedList.GenerateList(typedObjectDataList);

                if (objectsSchemeList.objects == null || objectsSchemeList.objects.Count == 0)
                {
                    TablesData.DeleteObjectsScheme();
                    Frame.Navigate(typeof(AuthorizePage));
                }
                else
                {
                    PopulateComboBoxAndMenuFlyOut();
                    PopulateListView();
                }
            }
            catch (Exception exception)
            {
                StopProgressRing();

                Debug.WriteLine(exception.Message);
            }
        }

        void PopulateComboBoxAndMenuFlyOut()
        {
            foreach(var objectScheme in objectsSchemeList.objects)
            {
                if (objectScheme.hidden)
                    continue;

                var comboBoxItem = CreateComboBoxItem(objectScheme);
                var menuFlyoutItem = CreateMenuFlyoutItem(objectScheme);

                ComboBoxType.Items.Add(comboBoxItem);
                AddObjectMenuFlyout.Items.Add(menuFlyoutItem);
                
                if (comboBoxItem.Name == selectedModelResource)
                {
                    ComboBoxType.SelectedItem = comboBoxItem;
                }
            }

            ComboBoxType.SelectionChanged += async (sender, arguments) =>
            {
                var selectedItem = (ComboBoxItem)ComboBoxType.SelectedItem;

                ApplicationData.Current.LocalSettings.Values[App.filterType] = selectedItem.Name;

                if (selectedItem == ComboBoxItemAllObjects)
                {
                    ListViewObjects.Items.Clear();
                    selectedModelResource = ComboBoxItemAllObjects.Name;

                    PopulateListView();
                }
                else
                {
                    var selectedModel = (ObjectModel)selectedItem.Tag;

                    var listGenericObjectsByEndpoint = GetGenericObjectList(selectedModel);
                    var listDraftPostsByObjectModel = new List<DraftPost>();

                    if(draftPostsList != null && draftPostsList.Count > 0)
                        listDraftPostsByObjectModel = draftPostsList.FindAll(draftPost => draftPost.genericObject.objectModelResourceUri == selectedModel.resource_uri);

                    if (listGenericObjectsByEndpoint != null && listGenericObjectsByEndpoint.listGenericObjects != null && listGenericObjectsByEndpoint.listGenericObjects.Count > 0 || listDraftPostsByObjectModel.Count > 0)
                    {
                        ListViewObjects.Items.Clear();
                        SetListViewItems(listGenericObjectsByEndpoint.listGenericObjects);
                        SetListViewItems(listDraftPostsByObjectModel);
                    }
                    else
                        await App.ShowMessageDialog("Нет объектов данного типа. Либо данные очищенны.");
                }
            };
       }

        async void PopulateListView()
        {
            StartProgressRing();
         
            if (typedGenericObjectList == null || typedGenericObjectList.Count == 0 )
            {
                typedObjectDataList = new List<TypedObjectData>();
                typedGenericObjectList = new List<GenericObjectTypedList>();

                if(objectsSchemeList == null)
                    objectsSchemeList = TablesData.objectsScheme ?? await TablesData.GetObjectsScheme();

                foreach (var objectModel in objectsSchemeList.objects)
                {
                    if (objectModel.hidden)
                        continue;

                    var objectsDataByModel = await ObjectsData.GetObjectsByEndpoint(objectModel.objects_uri);
                    var typedObjectdata = new TypedObjectData();

                    if (objectsDataByModel == null)
                        typedObjectdata = await TypedObjectData.GetTypedObjectdataByModel(objectModel);
                    else
                        typedObjectdata = new TypedObjectData(objectModel, objectsDataByModel);

                    if (typedObjectdata == null)
                    {
                        Debug.WriteLine(String.Format("Url: {0}; ObjectName: {1}; typedObjectData == null", objectModel.objects_uri, objectModel.name));
                        continue;
                    }

                    typedObjectdata.Save();

                    typedObjectDataList.Add(typedObjectdata);

                    var typedGenericObjects = new GenericObjectTypedList(typedObjectdata);
                    typedGenericObjectList.Add(typedGenericObjects);
                    Debug.WriteLine("Name: " + typedGenericObjects.verbose_name + "; Uri: " + typedGenericObjects.objects_uri);

                    if (typedGenericObjects == null || typedGenericObjects.listGenericObjects == null || typedGenericObjects.listGenericObjects.Count == 0)
                        continue;

                    if (selectedModelResource != ComboBoxItemAllObjects.Name && typedGenericObjects.name != selectedModelResource)
                        continue;

                    SetListViewItems(typedGenericObjects.listGenericObjects);
                }
            }
            else
            {
                foreach (var typedGenericObjects in typedGenericObjectList)
                {
                    if (typedGenericObjects.listGenericObjects == null)
                        continue;

                    if (selectedModelResource == ComboBoxItemAllObjects.Name || typedGenericObjects.resource_uri == selectedModelResource)
                        SetListViewItems(typedGenericObjects.listGenericObjects);
                }
            }

            if (draftPostsList != null && draftPostsList.Count > 0)
            {
                if (selectedModelResource == ComboBoxItemAllObjects.Name)
                {
                    SetListViewItems(draftPostsList);
                }
                else
                {
                    var listDraftPostsByObjectModel = new List<DraftPost>();
                    listDraftPostsByObjectModel = draftPostsList.FindAll(draftPost => draftPost.genericObject.objectModelResourceUri == selectedModelResource);

                    SetListViewItems(listDraftPostsByObjectModel);
                }
            }

            StopProgressRing();
        }
        
        void SetListViewItems(List<GenericObject> listGenericObjects)
        {
            foreach (var genericObject in listGenericObjects)
            {
                if(genericObject != null)
                    AddListViewItem(genericObject);
            }
        }

        void SetListViewItems(List<DraftPost> listDraftPosts)
        {
            foreach (var draftPost in listDraftPosts)
            {
                if (draftPost != null && draftPost.genericObject != null)
                    AddListViewItem(draftPost);
            }
        }

        void AddListViewItem(GenericObject genericObject)
        {
            var parentGrid = CreateListViewItem(genericObject);

            ListViewObjects.Items.Add(parentGrid);
        }

        void AddListViewItem(DraftPost draftPost)
        {
            var parentGrid = CreateListViewItem(draftPost.genericObject);
            parentGrid.Opacity = 0.3;
            parentGrid.Tag = draftPost;

            ListViewObjects.Items.Add(parentGrid);
        }

        Grid CreateListViewItem(GenericObject genericObject) 
        {
            var parentGrid = ListViewChildElements.CreateParentGrid(ListViewObjects);
            parentGrid.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(125, 0, 180, 170));
            
            var titleTextBlock = ListViewChildElements.CreateTitleTextBlock(genericObject.properties[namePropertyString].propertyValue.ToString());

            var dateTextBlock = new TextBlock();

            if(genericObject.properties[createdPropertyString].propertyValue != null)
                dateTextBlock = ListViewChildElements.CreateDatetimeTextBlock(Convert.ToDateTime(genericObject.properties[createdPropertyString].propertyValue.ToString()));
            else
                dateTextBlock = ListViewChildElements.CreateDatetimeTextBlock(DateTime.Now);

            var image = ListViewChildElements.CreateImageView(new Uri("ms-appx:///Assets/star.png"));

            parentGrid.Tag = genericObject;
            parentGrid.Children.Add(image);
            parentGrid.Children.Add(titleTextBlock);
            parentGrid.Children.Add(dateTextBlock);

            if (!genericObject.IsSended)
                parentGrid.Opacity = 0.6;

            return parentGrid;
        }

        GenericObjectTypedList GetGenericObjectList(ObjectModel objectModel)
        {
            try
            {
                if (typedGenericObjectList == null)
                    throw new Exception("Can't \"GetGenericObjectList\" because MainPage.typedGenericObject == null");

                foreach (var typedGeneric in typedGenericObjectList)
                {
                    if (typedGeneric == null)
                        throw new Exception("Can't \"GetGenericObjectList\" because MainPage.typedGenericObject[some object] == null");

                    if (typedGeneric.objects_uri == objectModel.objects_uri)
                        return typedGeneric;
                }

                return null;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);

                return null;
            }
        }

        ComboBoxItem CreateComboBoxItem(ObjectModel objectScheme)
        {
            var comboBoxItem = new ComboBoxItem();

            comboBoxItem.Name = objectScheme.resource_uri;
            comboBoxItem.Tag = objectScheme;
            comboBoxItem.Content = objectScheme.verbose_name;

            return comboBoxItem;
        }

        MenuFlyoutItem CreateMenuFlyoutItem(ObjectModel objectScheme)
        {
            var menuFlyoutItem = new MenuFlyoutItem();

            menuFlyoutItem.Name = objectScheme.resource_uri;
            menuFlyoutItem.Tag = objectScheme;
            menuFlyoutItem.Text = objectScheme.verbose_name;

            menuFlyoutItem.Click += (senser, e) =>
            {
                Frame.Navigate(typeof(CreateObjectPage), objectScheme);
            };

            return menuFlyoutItem;
        }

        void StartProgressRing()
        {
            progressRing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            progressRing.IsActive = true;
        }

        void StopProgressRing()
        {
            progressRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            progressRing.IsActive = false;
        }

        async Task<bool> UpdateEditedPosts()
        {
            if (typedGenericObjectList == null || typedGenericObjectList.Count == 0)
                return true;

            foreach (var typedGenericObjectData in typedGenericObjectList)
            {
                if (typedGenericObjectData.listGenericObjects == null || typedGenericObjectData.listGenericObjects.Count == 0)
                    continue;

                foreach (var generic in typedGenericObjectData.listGenericObjects)
                {
                    if (generic.IsSended)
                        continue;

                    var jsonString = GenericObject.GenerateJsonString(generic);

                    if (await RequestsHelper.UpdateObject(jsonString, generic.resourceUri))
                    {
                        generic.IsSended = true;
                        var objectData = new Objectdata(generic);
                        var objectModel = objectsSchemeList.objects.Find(x => x.resource_uri == typedGenericObjectData.resource_uri);

                        TypedObjectData.UpdateSingleObject(objectData, objectModel);
                    }
                }
            }

            return true;
        }

        async Task<bool> PostDrafts()
        {
            if (draftPostsList == null || draftPostsList.Count == 0)
                return true;

            foreach (var draftPost in draftPostsList)
            {
                var jsonString = GenericObject.GenerateJsonString(draftPost.genericObject);

                if (await RequestsHelper.PostObject(jsonString, draftPost.genericObject.objectModelResourceUri) != null)
                {
                    draftPost.genericObject.IsSended = true;
                    var objectModel = objectsSchemeList.objects.Find(x => x.resource_uri == draftPost.genericObject.objectModelResourceUri);

                    DraftPostsHelper.DeleteSingleDraftPost(draftPost);
                    TypedObjectData.AddSingleObject(draftPost.genericObject, objectModel);
                }
            }

            return true;
        }

        async Task<bool> UpdateObjectsDataFromNet()
        {
            try
            {
                foreach (var objectModel in objectsSchemeList.objects)
                {
                    if (objectModel.hidden)
                        continue;

                    var objectsDataByModel = await ObjectsData.GetObjectsByEndpoint(objectModel.objects_uri);

                    if (objectsDataByModel == null)
                    {
                        Debug.WriteLine(String.Format("Url: {0}; ObjectName: {1}; objectsDataByModel == null", objectModel.objects_uri, objectModel.name));
                        continue;
                    }

                    var typedObjectdata = new TypedObjectData(objectModel, objectsDataByModel);

                    if (typedObjectdata == null)
                    {
                        Debug.WriteLine(String.Format("Url: {0}; ObjectName: {1}; typedObjectData == null", objectModel.objects_uri, objectModel.name));
                        continue;
                    }

                    foreach (var objectData in objectsDataByModel.features)
                    {
                        await TypedObjectData.UpdateSingleObjectIfNotEdited(objectData, objectModel);
                    }
                }

                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Faild to UpdateObjectsDataFromNet(). Message: " + exception.Message);
                return false;
            }
        }

        /// <summary>
        /// Получает объект <see cref="NavigationHelper"/>, связанный с данным объектом <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Получает модель представлений для данного объекта <see cref="Page"/>.
        /// Эту настройку можно изменить на модель строго типизированных представлений.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Сохраняет состояние, связанное с данной страницей, в случае приостановки приложения или
        /// удаления страницы из кэша навигации.  Значения должны соответствовать требованиям сериализации
        /// <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">Источник события; как правило, <see cref="NavigationHelper"/></param>
        /// <param name="e">Данные события, которые предоставляют пустой словарь для заполнения
        /// сериализуемым состоянием.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region Регистрация NavigationHelper

        /// <summary>
        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// <para>
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="NavigationHelper.LoadState"/>
        /// и <see cref="NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.
        /// </para>
        /// </summary>
        /// <param name="e">Предоставляет данные для методов навигации и обработчики
        /// событий, которые не могут отменить запрос навигации.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);

            Frame.BackStack.Clear();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
