﻿using EcosRegistrator.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Devices.Geolocation;
using Windows.Web.Http;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EcosRegistrator
{
    public sealed partial class CreateObjectPage : Page, IFileOpenPickerContinuable
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        FileOpenPicker FilePicker { get; set; }
        Progress<HttpProgress> Progress { get; set; }
        CancellationTokenSource CancellationSource { get; set; }
        GridView gridViewImages { get; set; }
        GridView gridViewVideos { get; set; }
        BasicGeoposition currentPosition { get; set; }
        MapIcon userTappedLocationIcon { get; set; }
        ObjectModel objectModel { get; set; }
        GenericObject genericObject { get; set; }
        DraftPost draftPost { get; set; }

        public CreateObjectPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            CancellationSource = new CancellationTokenSource();
            Progress = new Progress<HttpProgress>();

            SetPageEvents();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
            Debug.WriteLine("Navigated to createObjPage");

            if (e.Parameter.GetType() == typeof(ObjectModel))
            {
                objectModel = (ObjectModel)e.Parameter;

                InitGenericOject();
            }
            else
            {
                draftPost = (DraftPost)e.Parameter;

                if (draftPost == null)
                    return;

                genericObject = draftPost.genericObject;
                objectModel = TablesData.objectsScheme.objects.Find(model => model.resource_uri == draftPost.genericObject.objectModelResourceUri);

                AppBarButtonDeleteDraft.Visibility = Windows.UI.Xaml.Visibility.Visible;
                FillControls();
            }

            if (objectModel == null)
                objectModel = await ObjectModel.GetWorkingObjectModel();

            if (objectModel == null)
                return;

            ObjectModel.SaveWorkingObjectModel(objectModel);
        }

        async void InitGenericOject()
        {
            if (objectModel != null)
            {
                genericObject = await GenericObject.GetWorkingGenericObject() ?? new GenericObject(objectModel);

                GenericObject.SaveWorkingGenericObject(genericObject);
            }

            FillControls();
        }

        void SetFilePickerTask(PickerLocationId locationId, List<string> listFileExtensions)
        {
            Debug.WriteLine("Setting FilePickerTask");

            FilePicker = new FileOpenPicker();
            FilePicker.SuggestedStartLocation = locationId;

            foreach (var item in listFileExtensions)
                FilePicker.FileTypeFilter.Add(item);
        }

        public void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs args)
        {
            var file = args.Files.FirstOrDefault();

            if (file == null)
                return;

            switch (file.FileType)
            {
                case ".bmp":
                case ".png":
                case ".jpeg":
                case ".jpg":
                    AddPhotoToListView(file);
                    break;
                case ".wmv":
                case ".mp4":
                case ".avi":
                case ".mov":
                case ".asf":
                case ".m2ts":
                    AddVideoToListView(file);
                    break;
                default:
                    break;
            }
        }

        async void AddPhotoToListView(StorageFile file)
        {
            if (gridViewImages == null)
            {
                gridViewImages = ListViewChildElements.CreateGridView("Фото");
                ListViewObjectFields.Items.Add(gridViewImages);
            }

            var parentGrid = ListViewChildElements.CreateGridForGridView(file);

            var fileStream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
            var bitmapImage = new BitmapImage();
            bitmapImage.SetSource(fileStream);

            var imageView = ListViewChildElements.CreateImageViewFromBitmapImage(bitmapImage);
            var buttonDeletePhoto = ListViewChildElements.CreateButtonDeletePhoto();

            buttonDeletePhoto.Click += (sender, args) =>
            {
                gridViewImages.Items.Remove(parentGrid);
            };

            parentGrid.Children.Add(imageView);
            parentGrid.Children.Add(buttonDeletePhoto);
            gridViewImages.Items.Add(parentGrid);
        }

        void AddVideoToListView(StorageFile file)
        {
            if (gridViewVideos == null)
            {
                gridViewVideos = ListViewChildElements.CreateGridView("Видео");
                ListViewObjectFields.Items.Add(gridViewVideos);
            }

            var parentGrid = ListViewChildElements.CreateGridForGridView(file);

            var bitmapImage = new BitmapImage(new Uri("ms-appx:///Assets/videoIcon.png"));
            var imageView = ListViewChildElements.CreateImageViewFromBitmapImage(bitmapImage);
            
            var buttonDeleteVideo = ListViewChildElements.CreateButtonDeletePhoto();

            buttonDeleteVideo.Click += (sender, args) =>
            {
                gridViewVideos.Items.Remove(parentGrid);
            };

            parentGrid.Children.Add(imageView);
            parentGrid.Children.Add(buttonDeleteVideo);
            gridViewVideos.Items.Add(parentGrid);
        }

        void SetPageEvents()
        {
            AppBarButtonPost.Click += async (sender, args) =>
            {
                Debug.WriteLine("Bar button post clicked!");
                var objectEndpoint = String.Empty;

                try
                {
                    DisactiveButtons();
                    
                    if (Convert.ToInt32(currentPosition.Latitude) == 0 && genericObject.geometry.coordinates == null && TextBoxLongitude.Text == String.Empty)
                        throw new Exception("Текущее местоположение еще не определенно! Попробуйте ввести вручную или отметить на карте.");

                    FillGenericObjectFields();

                    var postObjectJsonString = GenericObject.GenerateJsonString(genericObject);

                    objectEndpoint = await RequestsHelper.PostObject(postObjectJsonString, endpoint: objectModel.objects_uri);

                    if (objectEndpoint == null)
                        throw new Exception("PostObject failed.");

                    if (draftPost != null)
                       await DraftPostsHelper.DeleteSingleDraftPost(draftPost);

                    var justPostetObject = await RequestsHelper.GetSingleObject(new Uri(objectEndpoint, UriKind.Absolute));
                    await TypedObjectData.AddSingleObject(justPostetObject, objectModel);

                    Debug.WriteLine("object successfully posted!");
                }
                catch (Exception exception)
                {
                    Debug.WriteLine("Can't post object" + exception.Message);

#pragma warning disable
                    App.ShowMessageDialog("Не получается загрузить объект. " + exception.Message);
                }


                if (objectEndpoint == null || objectEndpoint == String.Empty)
                {
                    if (draftPost != null)
                    {
                        draftPost.genericObject = genericObject;
                        await DraftPostsHelper.UpdateSingleDraftPost(draftPost);
                    }
                    else
                    {
                        draftPost = new DraftPost(genericObject);
                        await DraftPostsHelper.AddSingleDraftPost(draftPost);
                    }
                }
                else
                {
                    if (await PostMedia(objectEndpoint, gridViewImages) && await PostMedia(objectEndpoint, gridViewVideos) && Frame.CanGoBack)
                        Frame.GoBack();
                }

                EnableButtons();
            };

            AppBarButtonAddPhoto.Click += (sender, args) =>
            {
                Debug.WriteLine("Bar button photo clicked!");

                SetFilePickerTask(
                   PickerLocationId.PicturesLibrary,
                   new List<string>
                    {
                        ".bmp",
                        ".png",
                        ".jpeg",
                        ".jpg"
                    }
                );

                if (FilePicker != null)
                    FilePicker.PickSingleFileAndContinue();
            };

            AppBarButtonAddVideo.Click += (sender, args) =>
            {
                Debug.WriteLine("Bar button video clicked!");

                SetFilePickerTask(
                   PickerLocationId.VideosLibrary,
                   new List<string>
                    {
                        ".wmv",
                        ".mp4",
                        ".avi",
                        ".mov",
                        ".asf",
                        ".m2ts"
                    }
                );

                if (FilePicker != null)
                    FilePicker.PickSingleFileAndContinue();
            };

            AppBarButtonDeleteDraft.Click += async (sender, args) =>
            {
                try
                {
                    await DraftPostsHelper.DeleteSingleDraftPost(draftPost);

                    Debug.WriteLine("draft successfully deleted");
                }
                catch(Exception exception)
                {
                    Debug.WriteLine("Can't delete draft! " + exception.Message);
                }

#pragma warning disable
                App.ShowMessageDialog("Черновик удален!");

                if (Frame.CanGoBack)
                    Frame.GoBack();
            };

            TextBlockTrack.Tapped += async (sender, srgs) =>
            {
                FillGenericObjectFields();
                GenericObject.SaveWorkingGenericObject(genericObject);

                Frame.Navigate(typeof(TrackPage), genericObject);
            };

            Progress.ProgressChanged += (s, e) =>
            {
                double percentSent = 100;

                if (e.TotalBytesToSend > 0)
                    percentSent = e.BytesSent * 100 / e.TotalBytesToSend.Value;

                progressBar.Value = percentSent;

                Debug.WriteLine(percentSent);
            };

            mapControl.MapTapped += (sender, args) =>
            {
                Geopoint tappedLocation;
                mapControl.GetLocationFromOffset(args.Position, out tappedLocation);

                if (userTappedLocationIcon != null)
                    mapControl.MapElements.Remove(userTappedLocationIcon);

                userTappedLocationIcon = MapHelper.AddObjectPoint(mapControl, new double[] { tappedLocation.Position.Longitude, tappedLocation.Position.Latitude });
                TextBoxLatitude.Text = tappedLocation.Position.Latitude.ToString();
                TextBoxLongitude.Text = tappedLocation.Position.Longitude.ToString();
            };
        }

        async void FillControls()
        {
            TextBlocObjectModelName.Text = objectModel.verbose_name;

            if (objectModel.icon == null)
                Debug.WriteLine("Objectmodel.Icon == null");
            else
                Debug.WriteLine("Write some code to set ObjectModel Icon");

            FillListView();

            if (genericObject.geometry.coordinates != null && genericObject.geometry.coordinates[0] != 0)
            {
                TextBoxLatitude.Text = genericObject.geometry.coordinates[1].ToString();
                TextBoxLongitude.Text = genericObject.geometry.coordinates[0].ToString();

                MapHelper.AddObjectPoint(mapControl, genericObject.geometry.coordinates);
                MapHelper.SetMapByCoordinates(mapControl, genericObject.geometry.coordinates);
            }
            else
            {
                SetMyCurrentPosition();
            }

            if (genericObject.track != null && genericObject.track.coordinates != null && genericObject.track.coordinates.Count > 0)
            {
                TextBlockDeleteTrack.Text = "\uE107";
                TextBlockTrack.Text = "Прикреплен трек";
            }
        }

        async void SetMyCurrentPosition()
        {
            currentPosition = await MapHelper.GetCurrenPosition();
            MapHelper.SetMapByCoordinates(mapControl, new double[] { currentPosition.Longitude, currentPosition.Latitude });
            MapHelper.AddMyLocationIcon(mapControl, new double[] { currentPosition.Longitude, currentPosition.Latitude });
            TextBoxLatitude.Text = currentPosition.Latitude.ToString();
            TextBoxLongitude.Text = currentPosition.Longitude.ToString();
        }

        void FillListView()
        {
            foreach (var field in objectModel.fields)
            {
                if (field.name == App.geometryFieldName || field.name == App.trackFieldName || field.name == App.imagesFieldName || field.name == App.videosFieldName)
                    continue;

                var genericObjectProperty = new GenericObjectProperty(field);

                if (genericObject.properties[field.name].propertyValue != null)
                    genericObjectProperty.propertyValue = genericObject.properties[field.name].propertyValue;

                switch (field.type)
                {
                    case FieldType.VarChar:
                        ListViewChildElements.CreateAndAddTextBoxFieldText(genericObjectProperty, ListViewObjectFields);
                        break;
                    case FieldType.Text:
                        ListViewChildElements.CreateAndAddTextBoxFieldText(genericObjectProperty, ListViewObjectFields, 120);
                        break;
                    case FieldType.DateTime:
                        if (field.name != "created")
                            ListViewChildElements.CreateDateTimeField(genericObjectProperty, ListViewObjectFields);
                        break;
                    case FieldType.Float:
                    case FieldType.Integer:
                        ListViewChildElements.CreateAndAddTextBoxFieldInteger(genericObjectProperty, ListViewObjectFields);
                        break;
                    default:
                        break;
                }
            }
        }

        void DisactiveButtons()
        {
            GridLoading.Visibility = Windows.UI.Xaml.Visibility.Visible;
            progressRing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
            progressRing.IsActive = true;
            AppBarButtonAddPhoto.IsEnabled = false;
            AppBarButtonAddVideo.IsEnabled = false;
            AppBarButtonPost.IsEnabled = false;
            mapControl.IsEnabled = false;
        }

        void EnableButtons()
        {
            GridLoading.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            progressRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            progressRing.IsActive = false;
            AppBarButtonAddPhoto.IsEnabled = true;
            AppBarButtonAddVideo.IsEnabled = true;
            AppBarButtonPost.IsEnabled = true;
            mapControl.IsEnabled = true;
        }

        void FillGenericObjectFields()
        {
            if (TextBoxLongitude.Text != null && TextBoxLatitude.Text != String.Empty && -180 < Convert.ToDouble(TextBoxLongitude.Text) &&
                Convert.ToDouble(TextBoxLongitude.Text) < 180 && Convert.ToDouble(TextBoxLatitude.Text) > -90 && Convert.ToDouble(TextBoxLatitude.Text) < 90)
            {
                genericObject.geometry.type = "Point";
                genericObject.geometry.coordinates = new double[] { Convert.ToDouble(TextBoxLongitude.Text), Convert.ToDouble(TextBoxLatitude.Text) };
            }
            else
            {
                genericObject.geometry.type = "Point";
                genericObject.geometry.coordinates = new double[] { 0, 0 };
            }

            foreach (var listViewItem in ListViewObjectFields.Items)
            {
                var listViewItemTypeName = listViewItem.GetType().Name;
                var genericProperty = new GenericObjectProperty();

                switch (listViewItemTypeName)
                {
                    case "TextBox":
                        var textBox = (TextBox)listViewItem;

                        genericProperty = (GenericObjectProperty)textBox.Tag;

                        if (genericProperty.name == null)
                            continue;

                        if ((genericProperty.type == FieldType.Integer || genericProperty.type == FieldType.Float) && textBox.Text == String.Empty)
                            continue;

                        genericObject.properties[genericProperty.name].propertyValue = textBox.Text;

                        Debug.WriteLine(textBox.Text);
                        break;
                    case "Grid":
                        var grid = (Grid)listViewItem;

                        if(grid.Name == GridCoordinates.Name || grid.Name == GridTrack.Name)
                            continue;

                        var datePiker = grid.Children[1] as DatePicker;
                        genericProperty = (GenericObjectProperty)grid.Tag;

                        genericObject.properties[genericProperty.name].propertyValue = datePiker.Date.ToString();

                        Debug.WriteLine(datePiker.Date);
                        break;
                    default:
                        break;
                }

                Debug.WriteLine(listViewItemTypeName);
            }
        }

        async Task<bool> PostMedia(string objectEndpoint, GridView gridView)
        {
            if (gridView == null || gridView.Items.Count == 0)
                return true;

            var splitLocationString = objectEndpoint.Split('/');
            var objectId = splitLocationString[splitLocationString.Length - 2];

            var files = GenerateFilesList(gridView);

            var field = new ObjectFields();

            if (gridView == gridViewVideos)
            {
                field = objectModel.fields.FirstOrDefault(x => x.name == App.videosFieldName);
            }
            else if (gridView == gridViewImages)
            {
                field = objectModel.fields.FirstOrDefault(x => x.name == App.imagesFieldName);
            }
            
            var endpoint = field.objects_uri;

            try
            {
                if (endpoint == null || endpoint == String.Empty)
                    throw new Exception("Wrong Endpoint! it's null!");

                //if (await RequestsHelper.PostMedia(files, objectId, endpoint, Progress))
                //    Debug.WriteLine("RequestsHelper.PostPhotos always return true! need to do something");
                //else
                //    Debug.WriteLine("some files may be not posted");
                var sendedFiles = 1;

                foreach (var file in files)
                {
                    TextBlockSendingFiles.Text = String.Format("{0}: {1}/{2}", gridView.Tag.ToString(), sendedFiles, files.Count);
                    
                    await RequestsHelper.PostFile(file, objectId, endpoint, Progress);

                    sendedFiles += 1;
                }
                
                var objectData = await RequestsHelper.GetSingleObject(new Uri(objectEndpoint, UriKind.Absolute));
                
                if (objectData != null)
                    await TypedObjectData.UpdateSingleObject(objectData, objectModel);
                else
                    Debug.WriteLine("Can't update Posted Object because can't get new objectData from net");

                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't upload photos. " + exception.Message);
                App.ShowMessageDialog("Произошла ошибка, файлы не загруженны. " + exception.Message);
                return false;
            }
        }

        List<StorageFile> GenerateFilesList(GridView gridView)
        {
            var files = new List<StorageFile>();

            foreach (var parentGrid in gridView.Items)
            {
                var grid = (Grid)parentGrid;
                var file = (StorageFile)grid.Tag;

                files.Add(file);
            }

            TextBlockSendingFiles.Text = "0/" + files.Count.ToString();

            return files;
        }

        /// <summary>
        /// Получает объект <see cref="NavigationHelper"/>, связанный с данным объектом <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Получает модель представлений для данного объекта <see cref="Page"/>.
        /// Эту настройку можно изменить на модель строго типизированных представлений.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Сохраняет состояние, связанное с данной страницей, в случае приостановки приложения или
        /// удаления страницы из кэша навигации.  Значения должны соответствовать требованиям сериализации
        /// <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">Источник события; как правило, <see cref="NavigationHelper"/></param>
        /// <param name="e">Данные события, которые предоставляют пустой словарь для заполнения
        /// сериализуемым состоянием.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region Регистрация NavigationHelper

        /// <summary>
        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// <para>
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="NavigationHelper.LoadState"/>
        /// и <see cref="NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.
        /// </para>
        /// </summary>
        /// <param name="e">Предоставляет данные для методов навигации и обработчики
        /// событий, которые не могут отменить запрос навигации.</param>

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
