﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Windows.Web.Http.Filters;
using Windows.Storage;

namespace EcosRegistrator
{
    class TokenManager
    {
        private const string tokenInfoFileName = "Token.txt";

        private async static Task<string> MakeToken()
        {
            using (var httpClient = new HttpClient())
            {
                try
                {
                    var currentEndpoint = new Uri(App.baseUrl + ApiEndpoints.GetToken, UriKind.Absolute);

                    var response = await httpClient.GetAsync(currentEndpoint);

                    if (response.StatusCode == HttpStatusCode.Ok)
                    {
                        var token = response.Content.ToString();
                        SaveToken(token);

                        return token;
                    }
                    else
                    {
                        throw new Exception( String.Format("Can't make token. Status code: {0}. Response message: {1}", response.StatusCode.ToString(), response.Content.ToString()) );
                    }
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(" Exception message: " + exception.Message);

                    return null;
                }
            }
        }

        private async static void SaveToken(string token)
        {
            var tokenDataFile = await GetTokenStorageFile();

            await FileIO.WriteTextAsync(tokenDataFile, token);
        }

        private async static Task<StorageFile> GetTokenStorageFile()
        {
            var temporaryFolder = ApplicationData.Current.TemporaryFolder;

            if (await App.FileExists(temporaryFolder, tokenInfoFileName))
            {
                var tokenDataFile = await temporaryFolder.GetFileAsync(tokenInfoFileName);

                return tokenDataFile;
            }
            else
            {
                var tokenDataFile = await temporaryFolder.CreateFileAsync(tokenInfoFileName);

                return tokenDataFile;
            }
        }

        public async static void DeleteToken()
        {
            var temporaryFolder = ApplicationData.Current.TemporaryFolder;

            if (await App.FileExists(temporaryFolder, tokenInfoFileName))
            {
                try
                {
                    var tokenDataFile = await temporaryFolder.GetFileAsync(tokenInfoFileName);
                    await tokenDataFile.DeleteAsync();

                    Debug.WriteLine("TokenDataFile successfully deleted!");
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.Message);
                }
            }
            else
            {
                Debug.WriteLine("TokenDataFile dosen't exist!");
            }
        }

        public async static Task<string> GetToken()
        {
            var tokenDataFile = await GetTokenStorageFile();
            var tokenString =  await FileIO.ReadTextAsync(tokenDataFile);

            if (tokenString == null || tokenString == "" || tokenString == "null")
                return await MakeToken();
            else
                return tokenString;
        }

        public async static Task<bool> IsTokenExist()
        {
            var tokenDataFile = await GetTokenStorageFile();
            var tokenString = await FileIO.ReadTextAsync(tokenDataFile);

            if (tokenString == null || tokenString == "" || tokenString == "null")
                return false;
            else
                return true;
        }
    }
}
