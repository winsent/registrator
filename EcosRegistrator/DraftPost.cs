﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    class DraftPost
    {
        public GenericObject genericObject { get; set; }
        public List<string> localImageUrls { get; set; }
        public List<string> localVideoUrls { get; set; }
        public DateTime PrimaryKey { get; set; }

        public DraftPost()
        {
            genericObject = new GenericObject();
            localImageUrls = new List<string>();
            localVideoUrls = new List<string>();
            PrimaryKey = DateTime.Now;
        }

        public DraftPost(GenericObject genericObject)
        {
            this.genericObject = genericObject;
            localImageUrls = new List<string>();
            localVideoUrls = new List<string>();
            PrimaryKey = DateTime.Now;
        }
    }

    class DraftPostsHelper
    {
        const string className = "DraftPostsHelper";

        public static async Task<bool> SaveList(List<DraftPost> listDraftPosts)
        {
            try 
            {
                var draftPostsJsonString = JsonConvert.SerializeObject(listDraftPosts);

                var dataFile = await App.localFolder.CreateFileAsync(className, CreationCollisionOption.OpenIfExists);
                await FileIO.WriteTextAsync(dataFile, draftPostsJsonString);

                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't save ListDraftPosts. " + exception.Message);
                return false;
            }
        }

        public static async Task<List<DraftPost>> GetList()
        {
            try
            {
                var dataFile = await App.localFolder.CreateFileAsync(className, CreationCollisionOption.OpenIfExists);
                var fileJsonString = await FileIO.ReadTextAsync(dataFile);

                if (fileJsonString == String.Empty)
                    throw new Exception("file is empty");

                var listDraftPosts = JsonConvert.DeserializeObject<List<DraftPost>>(fileJsonString);
                
                return listDraftPosts;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't get ListDraftPosts. " + exception.Message);
                return null;
            }
        }

        public static async void DeleteDraftPostsDataFile()
        {
            try
            {
                if (await App.FileExists(App.localFolder, className))
                {
                    var dataFile = await App.localFolder.GetFileAsync(className);
                    await dataFile.DeleteAsync();
                }

                Debug.WriteLine("DraftPostsDataFile deleted or not exist!");
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't delete DraftPostsDataFile. " + exception.Message);
            }
        }

        public static async Task<bool> AddSingleDraftPost(DraftPost draftPost)
        {
            try 
            {
                if (draftPost == null)
                    throw new Exception("Can't add draftPost because given post is null");

                var listDraftPosts = await GetList();

                if (listDraftPosts == null)
                    listDraftPosts = new List<DraftPost>();

                listDraftPosts.Add(draftPost);
                Debug.WriteLine("draftPost successfully added!");

                return await SaveList(listDraftPosts);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't add single DraftPost to list. " + exception.Message);
                return false;
            }
        }

        public static async Task<bool> DeleteSingleDraftPost(DraftPost draftPost)
        {
            try
            {
                var listDraftPosts = await GetList();

                if (listDraftPosts == null)
                    throw new Exception("List draftPosts is null.");

                var existedDraftPost = listDraftPosts.Find(post => post.PrimaryKey == draftPost.PrimaryKey);

                if (existedDraftPost == null)
                    throw new Exception("Can't find given draft post in listDraftPosts");

                listDraftPosts.Remove(existedDraftPost);
                Debug.WriteLine("draftPost successfully deleted!");

                return await SaveList(listDraftPosts);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Cant't delete single DraftPost" + exception.Message);
                return false;
            }
        }

        public static async Task<bool> UpdateSingleDraftPost(DraftPost draftPost)
        {
            try
            {
                var listDraftPosts = await GetList();

                if (listDraftPosts == null)
                    throw new Exception("List draftPosts is null.");

                var existedDraftPost = listDraftPosts.Find(post => post.PrimaryKey == draftPost.PrimaryKey);

                if (existedDraftPost != null)
                    listDraftPosts.Remove(existedDraftPost);

                listDraftPosts.Add(draftPost);
                Debug.WriteLine("draftPost successfully updated!");

                return await SaveList(listDraftPosts);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Cant't update single DraftPost" + exception.Message);
                return false;
            }
        }
    }
}
