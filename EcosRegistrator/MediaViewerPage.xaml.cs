﻿using EcosRegistrator.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Storage;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Основная страница" см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace EcosRegistrator
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MediaViewerPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        Uri fileUrl { get; set; }
        StorageFile storageFile { get; set; }
        string fileMimeType { get; set; }

        public MediaViewerPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            SetPageEvents();
        }

        void SetPageEvents()
        {
            ImageViewFile.ImageOpened += (sender, e) =>
            {
                ProcessRing.IsActive = false;
                ProcessRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            };
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);

            var parameterType = e.Parameter.GetType();

            if (parameterType != typeof(GenericImage) && parameterType != typeof(GenericVideo))
            {
                await App.ShowMessageDialog("Передан данный тип файлов не поддерживается");
                Debug.WriteLine("It's not image or video! e.Parameter type: " + e.Parameter.GetType());
                return;
            }

            if (parameterType == typeof(GenericImage))
            {
                MediaElementViewFile.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

                var genericImage = (GenericImage)e.Parameter;
                var fileEndpoint = genericImage.file;

                InitFileUrlAndMimeType(fileEndpoint);
                GetImageFile();
            }
            else
            {
                ImageViewFile.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                
                var genericVideo = (GenericVideo)e.Parameter;
                var fileEndpoint = genericVideo.file;

                InitFileUrlAndMimeType(fileEndpoint);

                var fileName = fileEndpoint.Split('/').Last();
                GetVideoFile(fileName);
            }
        }

        void InitFileUrlAndMimeType(string genericFileUrl)
        {
            if (genericFileUrl == null || genericFileUrl.ToString() == String.Empty)
            {
#pragma warning disable
                App.ShowMessageDialog("Ошибка, не передан адрес файла");
#pragma warning restore
                if (Frame.CanGoBack)
                    Frame.GoBack();
            }

            fileUrl = new Uri(String.Concat(App.baseUrl, genericFileUrl));
            fileMimeType = genericFileUrl.Split('.').Last();

            Debug.WriteLine("Getting file by url: " + String.Concat(App.baseUrl, genericFileUrl));
        }

        void GetImageFile()
        {
            var bitmapImage = new BitmapImage();
            var uri = fileUrl;
            bitmapImage.UriSource = uri;
            ImageViewFile.Source = bitmapImage;
        }

        async void GetVideoFile(string fileName)
        {
            var videoStorageFolder = KnownFolders.VideosLibrary;

            if (await App.FileExists(videoStorageFolder, fileName))
            {
                storageFile = await videoStorageFolder.GetFileAsync(fileName);
            }
            else
            {
                storageFile = await RequestsHelper.GetFile(fileUrl, fileName);
            }

            if (storageFile != null)
                SetMediaElement(storageFile);
            else
                Debug.WriteLine("Can't open file. storageFile is null");
        }

        async void SetMediaElement(StorageFile storageFile)
        {
            var stream = await storageFile.OpenAsync(FileAccessMode.Read);

            ProcessRing.IsActive = false;
            ProcessRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Debug.WriteLine("File opend");

            MediaElementViewFile.AutoPlay = true;
            MediaElementViewFile.SetSource(stream, fileMimeType);
            MediaElementViewFile.Play();
        }

        /// <summary>
        /// Получает объект <see cref="NavigationHelper"/>, связанный с данным объектом <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Получает модель представлений для данного объекта <see cref="Page"/>.
        /// Эту настройку можно изменить на модель строго типизированных представлений.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Сохраняет состояние, связанное с данной страницей, в случае приостановки приложения или
        /// удаления страницы из кэша навигации.  Значения должны соответствовать требованиям сериализации
        /// <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">Источник события; как правило, <see cref="NavigationHelper"/></param>
        /// <param name="e">Данные события, которые предоставляют пустой словарь для заполнения
        /// сериализуемым состоянием.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region Регистрация NavigationHelper

        /// <summary>
        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// <para>
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="NavigationHelper.LoadState"/>
        /// и <see cref="NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.
        /// </para>
        /// </summary>
        /// <param name="e">Предоставляет данные для методов навигации и обработчики
        /// событий, которые не могут отменить запрос навигации.</param>

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
