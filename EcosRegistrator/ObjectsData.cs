﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Windows.Web.Http.Filters;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    public class Objectdata
    {
        public Geometry geometry { get; set; }
        public int id { get; set; }
        public Dictionary<string, object> properties { get; set; }
        public string type { get; set; }
        public bool IsSended { get; set; }

        public Objectdata()
        {
            geometry = new Geometry();
            id = 0;
            properties = new Dictionary<string, object>();
            type = String.Empty;
            IsSended = true;
        }

        public Objectdata(GenericObject genericObject)
        {
            if (genericObject == null)
                throw new Exception("new Objectdata(GenericObject genericObject); given genericObject is null");
            
            if (genericObject.geometry == null || genericObject.geometry.coordinates == null || genericObject.geometry.coordinates.Length == 0)
                throw new Exception("new Objectdata(GenericObject genericObject); givengenericObject.geometry is null");

            geometry = genericObject.geometry;
            id = genericObject.id;
            type = "Feature";
            IsSended = genericObject.IsSended;

            properties = GenerateProperties(genericObject);
        }

        static Dictionary<string, object> GenerateProperties(GenericObject genericobject)
        {
            var properties = new Dictionary<string, object>();

            object propertyJsonString = null;

            if (genericobject.images != null && genericobject.images.Count > 0)
                propertyJsonString = JsonConvert.SerializeObject(genericobject.images);
            
            properties.Add(App.imagesFieldName, propertyJsonString);

            if (genericobject.videos != null && genericobject.videos.Count > 0)
            {
                var propertyJsonStringV = JsonConvert.SerializeObject(genericobject.videos);

                if (propertyJsonStringV != String.Empty)
                    properties.Add(App.videosFieldName, propertyJsonStringV);
            }
            else
                properties.Add(App.videosFieldName, null);

            if (genericobject.track != null)
                properties.Add(App.trackFieldName, JsonConvert.SerializeObject(genericobject.track));
            else
                properties.Add(App.trackFieldName, null);

            properties.Add(App.resourseUriFieldName, genericobject.resourceUri);

            foreach (var property in genericobject.properties)
            {
                properties.Add(property.Key, property.Value.propertyValue);
            }

            return properties;
        }
    }

    public class ObjectsData
    {
        public Crs crs { get; set; }
        public List<Objectdata> features { get; set; }
        public MetaDataModel meta { get; set; }
        public string type { get; set; }

        public static async Task<ObjectsData> GetObjectsByEndpoint(string endpoint)
        {
            try
            {
                var httpClient = new HttpClient();

                var currentEndpoint = new Uri(String.Format("{0}{1}", App.baseUrl, endpoint), UriKind.Absolute);
                var response = await httpClient.GetAsync(currentEndpoint);

                if (response.StatusCode == HttpStatusCode.Ok)
                {
                    //decod from utf-8
                    var buffer = await response.Content.ReadAsBufferAsync();
                    var byteArray = buffer.ToArray();
                    var responseString = System.Text.Encoding.UTF8.GetString(byteArray, 0, byteArray.Length);

                    var objectsData = JsonConvert.DeserializeObject<ObjectsData>(responseString);

                    return objectsData;
                }
                else if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    await App.ShowMessageDialog("Выбранный тип объектов не найден.");

                    return null;
                }
                else
                {
                    Debug.WriteLine(response.StatusCode.ToString());
                    //await App.ShowMessageDialog("Произошла ошибка. Попробуйте повторить запрос позднее");

                    return null;
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't get objectsData by endpoint: {0}; Message: {1}", endpoint, exception.Message);
                return null;
            }
        }
    }
}
