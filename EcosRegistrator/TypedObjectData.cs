﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    public class TypedObjectData
    {
        const string className = "TypedObjectData";

        public ObjectModel objectModel { get; set; }
        public ObjectsData objectsData { get; set; }

        public TypedObjectData()
        { }

        public TypedObjectData(ObjectModel objectModel, ObjectsData objectsData)
        {
            this.objectModel = objectModel;
            this.objectsData = objectsData;
        }

        public async Task<bool> Save()
        {
            try
            {
                if (this.objectModel == null || this.objectsData == null)
                    throw new Exception("Nothing to save! objectsData == null");

                var typedObjectsDataString = JsonConvert.SerializeObject(this);

                var typedObjectsDataFile = await App.localFolder.CreateFileAsync(String.Concat(className, this.objectModel.name), CreationCollisionOption.OpenIfExists);
                await FileIO.WriteTextAsync(typedObjectsDataFile, typedObjectsDataString);

                Debug.WriteLine("typedObjectsDataFile updated. objectModel.name = " + this.objectModel.name);
                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't save TypedObjectData to file. Exception message: " + exception.Message);
                return false;
            }
        }

        public static async Task<TypedObjectData> GetTypedObjectdataByModel(ObjectModel objectModel)
        {
            try
            {
                if (await App.FileExists(App.localFolder, String.Concat(className, objectModel.name)))
                {
                    Debug.WriteLine("File Exist! GetTypedObjectdataByModel(ObjectModel objectModel). objectModel: " + objectModel.name);
                    var typedObjectsDataFile = await App.localFolder.GetFileAsync(String.Concat(className, objectModel.name));
                    var typedObjectsDataString = await FileIO.ReadTextAsync(typedObjectsDataFile);

                    if (typedObjectsDataString != String.Empty)
                    {
                        var typedObjectsData = JsonConvert.DeserializeObject<TypedObjectData>(typedObjectsDataString);

                        Debug.WriteLine("We have typedObjectdata. objectModel: " + objectModel.name);
                        return typedObjectsData;
                    }
                    else
                        return null; //await GetObjectsFromNet();
                }
                else
                {
                    Debug.WriteLine("File NOT EXIST! Getting from net... GetTypedObjectdataByModel(ObjectModel objectModel). objectModel: " + objectModel.name);
                    var objectsData = await ObjectsData.GetObjectsByEndpoint(objectModel.objects_uri);
                    var typedObjectsData = new TypedObjectData(objectModel, objectsData);

#pragma warning disable
                    typedObjectsData.Save();

                    return typedObjectsData;
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't get typed data GetTypedObjectdataByModel(ObjectModel objectModel)" + exception.Message);
                return null;
            }
        }

        public static async Task<List<TypedObjectData>> GenerateList()
        {
            try
            {
                var typedObjectDataList = new List<TypedObjectData>();

                if (TablesData.objectsScheme == null)
                    await TablesData.GetObjectsScheme();

                if (TablesData.objectsScheme == null || TablesData.objectsScheme.objects == null)
                    throw new Exception("Can't get objectScheme; TablesData.objectsScheme.objects == null");

                foreach (var objectModel in TablesData.objectsScheme.objects)
                {
                    if (objectModel.hidden)
                        continue;

                    var typedObjectdata = await GetTypedObjectdataByModel(objectModel);

                    if (typedObjectdata != null)
                        typedObjectDataList.Add(typedObjectdata);
                }

                return typedObjectDataList;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return null;
            }
        }

        public static async void DeleteObjectsData(ObjectModel objectModel)
        {
            try
            {
                if (objectModel == null)
                    throw new Exception("TypedObjectdata.DeleteObjectsData method; given objectModel == null");

                if (await App.FileExists(App.localFolder, String.Concat(className, objectModel.name)))
                {
                    var tablesShemeDataFile = await App.localFolder.GetFileAsync(String.Concat(className, objectModel.name));
                    await tablesShemeDataFile.DeleteAsync();
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(" Error: " + exception.Message);
            }
        }

        public static async Task<bool> DeleteSingleObject(GenericObject genericObject)
        {
            try
            {
                //Making ObjectModel.objects_uri from GenericObject.resourceUri to find what type of ObjectModel is a GenericObject
                var objectsModelObjectsUri = genericObject.resourceUri.Substring(0, genericObject.resourceUri.Length - genericObject.id.ToString().Length - 1);
                var objectModel = TablesData.objectsScheme.objects.Find(x => x.objects_uri == objectsModelObjectsUri);

                Debug.WriteLine("DeleteSingleObject(generic). Needed objectModel: " + objectModel.name);
                var typedObjectData = await GetTypedObjectdataByModel(objectModel);

                if (typedObjectData == null)
                    throw new Exception("DeleteSingleObject(GenericObject genericObject). typedObjectData == null");

                var objectData = typedObjectData.objectsData.features.Find(x => x.id == genericObject.id);
                typedObjectData.objectsData.features.Remove(objectData);

                return await typedObjectData.Save();
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return false;
            }
        }

        public static async Task<bool> AddSingleObject(Objectdata objectData, ObjectModel objectModel)
        {
            try
            {
                var typedObjectData = await GetTypedObjectdataByModel(objectModel);

                if (typedObjectData == null)
                    throw new Exception("DeleteSingleObject(GenericObject genericObject). typedObjectData == null");

                typedObjectData.objectsData.features.Add(objectData);

                return await typedObjectData.Save();
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Fail TypedObjectData.AddSingleObject(GenericObject). Message: " + exception.Message);
                return false;
            }
        }

        public static async Task<bool> AddSingleObject(GenericObject genericObject, ObjectModel objectModel)
        {
            try
            {
                var typedObjectData = await GetTypedObjectdataByModel(objectModel);

                if (typedObjectData == null)
                    throw new Exception("DeleteSingleObject(GenericObject genericObject). typedObjectData == null");

                var objectData = new Objectdata(genericObject);
                typedObjectData.objectsData.features.Add(objectData);

                return await typedObjectData.Save();
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Fail TypedObjectData.AddSingleObject(GenericObject). Message: " + exception.Message);
                return false;
            }
        }

        public static async Task<bool> UpdateSingleObject(Objectdata objectData, ObjectModel objectModel)
        {
            try
            {
                var typedObjectData = await GetTypedObjectdataByModel(objectModel);

                if (typedObjectData == null)
                    throw new Exception("DeleteSingleObject(GenericObject genericObject). typedObjectData == null");

                var objectForUpdate = typedObjectData.objectsData.features.Find(x => x.id == objectData.id);

                if (objectForUpdate != null)
                    typedObjectData.objectsData.features.Remove(objectForUpdate);

                typedObjectData.objectsData.features.Add(objectData);

                return await typedObjectData.Save();
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Fail TypedObjectData.UpdateSingleObject(Objectdata, ObjectModel). Message: " + exception.Message);
                return false;
            }
        }

        public static async Task<bool> UpdateSingleObjectIfNotEdited(Objectdata objectData, ObjectModel objectModel)
        {
            try
            {
                var typedObjectData = await GetTypedObjectdataByModel(objectModel);

                if (typedObjectData == null)
                    throw new Exception("DeleteSingleObject(GenericObject genericObject). typedObjectData == null");

                var objectForUpdate = typedObjectData.objectsData.features.Find(x => x.id == objectData.id);

                if (objectForUpdate != null && !objectForUpdate.IsSended)
                    return true; // TODO: maybe need to throw exception

                if (objectForUpdate != null)
                    typedObjectData.objectsData.features.Remove(objectForUpdate);

                typedObjectData.objectsData.features.Add(objectData);

                return await typedObjectData.Save();
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Fail TypedObjectData.UpdateSingleObjectIfNotEdited(Objectdata, ObjectModel). Message: " + exception.Message);
                return false;
            }
        }
    }
}
