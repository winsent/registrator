﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Windows.Web.Http.Filters;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    class RequestsHelper
    {
        static HttpClient httpClient { get; set; }

        static void InitHttpClient()
        {
            //if (httpClient == null)
            //    httpClient = new HttpClient();
            //if (httpClient != null)
            //    httpClient.Dispose();

            httpClient = new HttpClient();
        }
        
        public static async Task<string> PostObject(string jsonObjectString, string endpoint)
        {
            try
            {
                InitHttpClient();

                var content = new HttpStringContent(jsonObjectString, Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");

                var response = await httpClient.PostAsync(new Uri(String.Concat(App.baseUrl, endpoint)), content);

                if (!response.IsSuccessStatusCode)
                    throw new Exception("Can't post object. StatusCode: " + response.StatusCode.ToString());

                return response.Headers["Location"].ToString();
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                
                return null;
            }
        }

        public static async Task<bool> Autorization(string UserName, string UserPassword)
        {
            InitHttpClient();
            //var uri = new Uri("http://188.166.97.214:8000/login/");
            var uri = new Uri(String.Format("{0}{1}", App.baseUrl, ApiEndpoints.Authtorisation));

            //var token = await TokenManager.GetToken();
            //httpClient.DefaultRequestHeaders.Add("X-CSRFToken", token);
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("password", UserPassword));
            keyValues.Add(new KeyValuePair<string, string>("username", UserName));

            var content = new HttpFormUrlEncodedContent(keyValues);

            try
            {
                var response = await httpClient.PostAsync(uri, content);

                if (!response.IsSuccessStatusCode)
                    throw new Exception(String.Format("Не правельный логин или пароль. Status code: {0}", response.StatusCode.ToString()));

                return true;
            }
            catch(Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return false;
            }
        }

        public static async Task<bool> PostMedia(List<StorageFile> files, string objectId, string endpoint, Progress<HttpProgress> Progress)
        {
            var filesCount = files.Count;

            foreach(var file in files)
            {
                if (await PostFile(file, objectId, endpoint, Progress))
                    Debug.WriteLine("File successfully uploaded!");
                else
                    Debug.WriteLine("uploading fail failed!");

                filesCount -= 1;
                Debug.WriteLine("Waiting {0} files to upload", filesCount);
            }

            return true;
        }

        public static async Task<bool> PostFile(StorageFile file, string objectId, string endpoint, Progress<HttpProgress> Progress)
        {
            try
            {
                InitHttpClient();
                var CancellationSource = new System.Threading.CancellationTokenSource();

                Debug.WriteLine("Start sending");

                var fileStream = await file.OpenAsync(FileAccessMode.Read);
                var stream = fileStream.AsStream();
                var streamContent = new HttpStreamContent(stream.AsInputStream());

                //Image or Video
                var form = new HttpMultipartFormDataContent();
                form.Add(streamContent, "file", String.Format(file.Name, Guid.NewGuid().ToString()));

                //objectId
                var stringContent = new HttpStringContent(objectId, Windows.Storage.Streams.UnicodeEncoding.Utf8);
                form.Add(stringContent, "object_id");

                var uri = new Uri(String.Concat(App.baseUrl, endpoint));

                var response = await httpClient.PostAsync(uri, form).AsTask(CancellationSource.Token, Progress);

                if (response.IsSuccessStatusCode)
                {
                    Debug.WriteLine("File uploaded! Status code: " + response.StatusCode.ToString());
                    return true;
                }

                Debug.WriteLine("File Not uploaded! Status code: " + response.StatusCode.ToString());
                return false;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("File Not uploaded!" + exception.Message);
                return false;
            }
        }

        public static async Task<bool> DeletePost(Uri url)
        {
            try
            {
                InitHttpClient();

                var response = await httpClient.DeleteAsync(url);

                if (response.StatusCode == HttpStatusCode.NoContent)
                    return true;
                
                Debug.WriteLine(response.StatusCode);
                return false;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return false;
            }
        }

        public static async Task<Objectdata> GetSingleObject(Uri url)
        {
            try
            {
                InitHttpClient();

                Debug.WriteLine("Getting single object. url: " + url.ToString());

                var response = await httpClient.GetAsync(url);

                if (!response.IsSuccessStatusCode)
                    throw new Exception("Can't get single object. StatusCode: " + response.StatusCode.ToString());

                Debug.WriteLine(response.StatusCode);

                if (response.Content.ToString() == String.Empty)
                    throw new Exception("response Content == null");

                //decod from utf-8
                var buffer = await response.Content.ReadAsBufferAsync();
                var byteArray = buffer.ToArray();
                var responseString = System.Text.Encoding.UTF8.GetString(byteArray, 0, byteArray.Length);

                var objectData = JsonConvert.DeserializeObject<Objectdata>(responseString);

                Debug.WriteLine("get single objectData successfully. obj.id: " + objectData.id);
                return objectData;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return null;
            }
        }

        public static async Task<bool> UpdateObject(string JsonContentString, string endpoint)
        {
            try
            {
                InitHttpClient();

                var url = new Uri(String.Concat(App.baseUrl, endpoint), UriKind.Absolute);
                var content = new HttpStringContent(JsonContentString, Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");

                Debug.WriteLine("Updatinging single object. Url: " + url.ToString());
                
                var response = await httpClient.PutAsync(url, content);

                if (!response.IsSuccessStatusCode)
                    throw new Exception("Can't update object. Status code: " + response.StatusCode);

                Debug.WriteLine("Single object successfully UPDATED. Url: {0} StatusCode: {1}", url.ToString(), response.StatusCode);
                return true; 
            }
            catch (Exception exception)
            { 
                Debug.WriteLine("Can't update post! Message: {0}", exception.Message);
                return false;
            }
        }

        public static async Task<bool> DeleteFile(string endpoint)
        {
            try
            {
                InitHttpClient();

                var url = new Uri(String.Concat(App.baseUrl, endpoint), UriKind.Absolute);

                var response = await httpClient.DeleteAsync(url);

                if (response.StatusCode == HttpStatusCode.MethodNotAllowed)
                    throw new Exception("Отказанно в доступе! Нужно авторизоваться. Status code: {0}" + response.StatusCode);

                if (response.StatusCode == HttpStatusCode.NotFound)
                    Debug.WriteLine("File already deleted. Status code: {0}", response.StatusCode);

                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return false;
            }
        }

        public static async Task<StorageFile> GetFile(Uri fileUrl, string fileName)
        {
            Debug.WriteLine("Getting file from url: " + fileUrl);
            StorageFile destinationFile;

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.GetAsync(fileUrl);

                    if (response.StatusCode == HttpStatusCode.Ok)
                    {
                        // There is need to take STREAM and write it into the file
                        var fileContent = await response.Content.ReadAsBufferAsync();

                        destinationFile = await KnownFolders.VideosLibrary.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

                        using (var writeStream = await destinationFile.OpenAsync(FileAccessMode.ReadWrite))
                        using (var outputStream = writeStream.GetOutputStreamAt(0))
                        using (var dataWriter = new DataWriter(outputStream))
                        using (var input = fileContent.AsStream())
                        {
                            var buffer = new byte[1024];
                            var totalSize = 0;

                            for (int size = input.Read(buffer, 0, buffer.Length); size > 0; size = input.Read(buffer, 0, buffer.Length))
                            {
                                dataWriter.WriteBytes(buffer);
                                totalSize += size;
                            }

                            await dataWriter.StoreAsync();
                            await outputStream.FlushAsync();

                            dataWriter.DetachStream();

                            Debug.WriteLine(destinationFile.Path);
                            return destinationFile;
                        }
                    }
                    else
                    {
                        throw new Exception("Status code: " + response.StatusCode.ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't get file. " + exception.Message);
                return null;
            }
        }
    }
}
