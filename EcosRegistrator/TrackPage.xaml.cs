﻿using EcosRegistrator.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Geolocation;

// Документацию по шаблону элемента "Основная страница" см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace EcosRegistrator
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class TrackPage : Page
    {
        const char separator = ';';

        NavigationHelper navigationHelper;
        ObservableDictionary defaultViewModel = new ObservableDictionary();
        GenericObject genericObject { get; set; }
        Geolocator geolocator { get; set; }
        DispatcherTimer timer = new DispatcherTimer();
        MapPolyline line;
        List<BasicGeoposition> listBasicGeoposition;
        long startTime;
        
        public TrackPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            listBasicGeoposition = new List<BasicGeoposition>();
            timer.Interval = TimeSpan.FromSeconds(1);
            geolocator = MapHelper.GetGeolocator();

            SetPageEvents();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);

            if (e.Parameter.GetType() == typeof(GenericObject))
                genericObject = (GenericObject)e.Parameter;
            else
                genericObject = await GenericObject.GetWorkingGenericObject();

            if (genericObject == null)
                return;

            FillControls();
            DetectCurrentGeoPosition();
        }

        void FillControls()
        {
            if (genericObject.track == null || genericObject.track.coordinates == null || genericObject.track.coordinates.Count == 0)
                return;

            MapHelper.SetMapByCoordinates(Map, genericObject.track.coordinates[0]);

            foreach (var coordinate in genericObject.track.coordinates)
            {
                var newBasicGeoposition = new BasicGeoposition
                {
                    Latitude = coordinate[1],
                    Longitude = coordinate[0]
                };

                listBasicGeoposition.Add(newBasicGeoposition);
            
                var textBlock = CreateTextBlockCoordinates(coordinate);
                ListViewCoordinates.Items.Add(textBlock);
            }

            TextBlockDistance.Text = String.Format("{0} м", ((listBasicGeoposition.Count - 1) * geolocator.MovementThreshold));
            line = MapHelper.InitMapLine(listBasicGeoposition);
            Map.MapElements.Add(line);

            ButtonDeleteTrack.Visibility = Windows.UI.Xaml.Visibility.Visible;
            ButtonStart.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        void SetPageEvents()
        {
            timer.Tick += (sender, args) =>
            {
                TimeSpan runTime = TimeSpan.FromMilliseconds(System.Environment.TickCount - startTime);
                TextBlockTime.Text = runTime.ToString(@"hh\:mm\:ss");
            };

            ButtonDeleteTrack.Click += (sender, args) =>
            {
                ListViewCoordinates.Items.Clear();

                if (line != null)
                    Map.MapElements.Remove(line);

                line = null;

                ButtonDeleteTrack.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                ButtonStart.Visibility = Windows.UI.Xaml.Visibility.Visible;
            };

            ButtonStart.Click += async (sender, args) =>
            {
                if (timer.IsEnabled)
                {
                    timer.Stop();
                    geolocator.PositionChanged -= PositionCangedEvent;
                    listBasicGeoposition = null;

                    ButtonStart.Content = "Start";
                    ButtonStart.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    ButtonDeleteTrack.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else 
                {
                    TextBlockDistance.Text = "0 м";
                    TextBlockTime.Text = "00:00:00";

                    if (listBasicGeoposition == null)
                    {
                        ProgressRingWait.Visibility = Windows.UI.Xaml.Visibility.Visible;
                        ProgressRingWait.IsActive = true;

                        listBasicGeoposition = new List<BasicGeoposition>();

                        var currentPosition = await MapHelper.GetCurrenPosition();
                        listBasicGeoposition.Add(currentPosition);
                    }

                    line = MapHelper.InitMapLine(listBasicGeoposition);
                    geolocator.PositionChanged += PositionCangedEvent;

                    timer.Start();
                    startTime = System.Environment.TickCount;
                    ButtonStart.Content = "Stop";

                    ProgressRingWait.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    ProgressRingWait.IsActive = false;
                }
            };

            AppBarButtoCancelTrack.Click += (sender, args) =>
            {
                if (Frame.CanGoBack)
                    Frame.GoBack();
            };

            AppBarButtonAceptTrack.Click += async(sender, args) =>
            {
                if (ListViewCoordinates.Items.Count > 0)
                {
                    genericObject.track.coordinates = GenerateTrack();
                    genericObject.track.type = "LineString";
                }
                else
                    genericObject.track = null;

                await GenericObject.SaveWorkingGenericObject(genericObject);

                if (Frame.CanGoBack)
                    Frame.GoBack();
            };
        }

        async void PositionCangedEvent(object sender, PositionChangedEventArgs args)
        {
            if (geolocator.LocationStatus != PositionStatus.Ready)
                return;

            var newBasicGeoposition = new BasicGeoposition
            {
                Latitude = args.Position.Coordinate.Point.Position.Latitude,
                Longitude = args.Position.Coordinate.Point.Position.Longitude
            };

            listBasicGeoposition.Add(newBasicGeoposition);
            var path = new Geopath(listBasicGeoposition);
            
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                Map.Center = new Geopoint(newBasicGeoposition);
                TextBlockDistance.Text = String.Format("{0} м", ((listBasicGeoposition.Count - 1) * geolocator.MovementThreshold));
                line.Path = path;
                Map.MapElements.Add(line);

                var textBlock = CreateTextBlockCoordinates(new double[] { newBasicGeoposition.Longitude, newBasicGeoposition.Latitude });
                ListViewCoordinates.Items.Add(textBlock);
            });
        }

        async void DetectCurrentGeoPosition()
        {
            var currentPosition = await MapHelper.GetCurrenPosition();
            MapHelper.SetMapByCoordinates(Map, new double[] {currentPosition.Longitude, currentPosition.Latitude});

            listBasicGeoposition.Add(currentPosition);
            ButtonStart.IsEnabled = true;
        }

        List<double[]> GenerateTrack()
        {
            var trackCoordinates = new List<double[]>();

            foreach (var item in ListViewCoordinates.Items)
            {
                var textblock = (TextBlock)item;
                //var stringCoordinates = textblock.Text;
                //var separatedString = stringCoordinates.Split(separator);
                ////costil
                //var doubleCoordinates = new double[] { Convert.ToDouble(separatedString[0]), Convert.ToDouble(separatedString[1]) };
                var doubleCoordinates = (double[])textblock.Tag;

                trackCoordinates.Add(doubleCoordinates);
            }

            return trackCoordinates;
        }

        TextBlock CreateTextBlockCoordinates(double[] coordinate)
        {
            var textBlock = new TextBlock();
            textBlock.FontSize = 26;
            textBlock.Margin = new Thickness(15, 5, 0, 0);
            textBlock.Tag = coordinate;
            textBlock.Text = String.Format("{0}{1} {2}", coordinate[0], separator, coordinate[1]);

            return textBlock;
        }

        /// <summary>
        /// Получает объект <see cref="NavigationHelper"/>, связанный с данным объектом <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Получает модель представлений для данного объекта <see cref="Page"/>.
        /// Эту настройку можно изменить на модель строго типизированных представлений.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Сохраняет состояние, связанное с данной страницей, в случае приостановки приложения или
        /// удаления страницы из кэша навигации.  Значения должны соответствовать требованиям сериализации
        /// <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">Источник события; как правило, <see cref="NavigationHelper"/></param>
        /// <param name="e">Данные события, которые предоставляют пустой словарь для заполнения
        /// сериализуемым состоянием.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region Регистрация NavigationHelper

        /// <summary>
        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// <para>
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="NavigationHelper.LoadState"/>
        /// и <see cref="NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.
        /// </para>
        /// </summary>
        /// <param name="e">Предоставляет данные для методов навигации и обработчики
        /// событий, которые не могут отменить запрос навигации.</param>

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
