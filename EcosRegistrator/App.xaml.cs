﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using Windows.Storage;
using EcosRegistrator.Common;
#endregion

// Документацию по шаблону "Пустое приложение" см. по адресу http://go.microsoft.com/fwlink/?LinkId=391641

namespace EcosRegistrator
{
    /// <summary>
    /// Обеспечивает зависящее от конкретного приложения поведение, дополняющее класс Application по умолчанию.
    /// </summary>
    public sealed partial class App : Application
    {
        public static string geometryFieldName = "geometry";
        public static string trackFieldName = "track";
        public static string imagesFieldName = "images";
        public static string videosFieldName = "videos";
        public static string resourseUriFieldName = "resource_uri";
        public static string filterType = "filterType";

        public static string baseUrl { get; set; }
        public static StorageFolder localFolder { get; set; }
        //public static Style textBoxTransparentStyle { get; set; }

        public ContinuationManager continuationManager { get; private set; }

        private TransitionCollection transitions;

        /// <summary>
        /// Инициализирует одноэлементный объект приложения.  Это первая выполняемая строка разрабатываемого
        /// кода; поэтому она является логическим эквивалентом main() или WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;

            localFolder = ApplicationData.Current.LocalFolder;

            ApplicationData.Current.LocalSettings.Values.Remove(filterType);
            //textBoxTransparentStyle = Application.Current.Resources["TransparentStyle"] as Style;

            GenericObject.DeleteWorkingGenericObject();
            ObjectModel.DeleteWorkingObjectModel();
        }

        private Frame CreateRootFrame()
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                rootFrame.CacheSize = 3;

                // Set the default language
                //rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];
                rootFrame.NavigationFailed += OnNavigationFailed;

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            return rootFrame;
        }

        private async Task RestoreStatusAsync(ApplicationExecutionState previousExecutionState)
        {
            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (previousExecutionState == ApplicationExecutionState.Terminated)
            {
                // Restore the saved session state only when appropriate
                try
                {
                    await SuspensionManager.RestoreAsync();
                }
                catch (SuspensionManagerException)
                {
                    //Something went wrong restoring state.
                    //Assume there is no state and continue
                }
            }
        }

        #if WINDOWS_PHONE_APP
        /// <summary>
        /// Handle OnActivated event to deal with File Open/Save continuation activation kinds
        /// </summary>
        /// <param name="e">Application activated event arguments, it can be casted to proper sub-type based on ActivationKind</param>
        protected async override void OnActivated(IActivatedEventArgs e)
        {
            base.OnActivated(e);

            continuationManager = new ContinuationManager();

            Frame rootFrame = CreateRootFrame();
            await RestoreStatusAsync(e.PreviousExecutionState);


            if (rootFrame.Content == null)
            {
                rootFrame.Navigate(typeof(MainPage));
            }

            var continuationEventArgs = e as IContinuationActivatedEventArgs;
            if (continuationEventArgs != null)
            {

                // Call ContinuationManager to handle continuation activation
                continuationManager.Continue(continuationEventArgs, rootFrame);

            }

            Window.Current.Activate();
        }
#endif

        /// <summary>
        /// Вызывается при обычном запуске приложения пользователем.  Будут использоваться другие точки входа,
        /// если приложение запускается для открытия конкретного файла, отображения
        /// результатов поиска и т. д.
        /// </summary>
        /// <param name="e">Сведения о запросе и обработке запуска.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = CreateRootFrame();
            await RestoreStatusAsync(e.PreviousExecutionState);

            if (rootFrame.Content == null)
            {
                // Удаляет турникетную навигацию для запуска.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;

                // Если стек навигации не восстанавливается для перехода к первой странице,
                // настройка новой страницы путем передачи необходимой информации в качестве параметра
                // навигации
                //if (!rootFrame.Navigate(typeof(MainPage), e.Arguments))
                //{
                //    throw new Exception("Failed to create initial page");
                //}
                
                if (ApplicationData.Current.LocalSettings.Values["BaseUrl"] != null)
                {
                    baseUrl = ApplicationData.Current.LocalSettings.Values["BaseUrl"].ToString();

                    var uri = new Uri(String.Format("{0}{1}", baseUrl, ApiEndpoints.Authtorisation));
                    var bpf = new Windows.Web.Http.Filters.HttpBaseProtocolFilter();
                    var cookie = bpf.CookieManager.GetCookies(uri);

                    //var temporaryFolder = ApplicationData.Current.TemporaryFolder;

                    //if (!await FileExists(temporaryFolder, "Token.txt"))
                    //    await temporaryFolder.CreateFileAsync("Token.txt");

                    if (cookie.Count > 0)
                        rootFrame.Navigate(typeof(MainPage), e.Arguments);
                    else
                        rootFrame.Navigate(typeof(AuthorizePage), e.Arguments);
                }
                else
                    rootFrame.Navigate(typeof(AuthorizePage), e.Arguments);
            }

            // Обеспечение активности текущего окна
            Window.Current.Activate();
        }

        /// <summary>
        /// Восстанавливает переходы содержимого после запуска приложения.
        /// </summary>
        /// <param name="sender">Объект, где присоединен обработчик.</param>
        /// <param name="e">Сведения о событии перехода.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }

        /// <summary>
        /// Вызывается при приостановке выполнения приложения.  Состояние приложения сохраняется
        /// без учета информации о том, будет ли оно завершено или возобновлено с неизменным
        /// содержимым памяти.
        /// </summary>
        /// <param name="sender">Источник запроса приостановки.</param>
        /// <param name="e">Сведения о запросе приостановки.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            // TODO: Сохранить состояние приложения и остановить все фоновые операции
            await SuspensionManager.SaveAsync();
            deferral.Complete();
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        public static async Task<bool> FileExists(StorageFolder folder, string fileName)
        {
            return (await folder.GetFilesAsync()).Any(x => x.Name == fileName);
        }

        public static async Task<IUICommand> ShowMessageDialog(string content)
        {
            if (content != null || content.Length == 0)
            {
                var message = new MessageDialog(content);
                return await message.ShowAsync();
            }

            throw new Exception("You didn't have specified the input content for new message dialog.");
        }

        public static string GetBuildedDateTimeString(DateTime datetime)
        {
            var stringBuilder = new System.Text.StringBuilder();

            if (datetime.Day < 10)
                stringBuilder.Append("0");

            stringBuilder.Append(datetime.Day);
            stringBuilder.Append('.');

            if (datetime.Month < 10)
                stringBuilder.Append("0");

            stringBuilder.Append(datetime.Month);
            stringBuilder.Append('.');
            stringBuilder.Append(datetime.Year);
            stringBuilder.Append(' ');

            if (datetime.Hour < 10)
                stringBuilder.Append("0");

            stringBuilder.Append(datetime.Hour);
            stringBuilder.Append(':');

            if (datetime.Minute < 10)
                stringBuilder.Append("0");

            stringBuilder.Append(datetime.Minute);
            return stringBuilder.ToString();
        }
    }
}