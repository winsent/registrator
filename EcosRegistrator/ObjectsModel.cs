﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    public class ObjectsModel
    {
        public MetaDataModel meta { get; set; }
        public List<ObjectModel> objects { get; set; }
    }

    public class MetaDataModel
    {
        public int limit { get; set; }
        public int? next { get; set; }
        public int offset { get; set; }
        public int? previous { get; set; }
        public int total_count { get; set; }
    }

    public class ObjectModel
    {
        const string workingObjectModelFileName = "workingObjectModel";

        public List<ObjectFields> fields { get; set; }
        public bool hidden { get; set; }
        public string icon { get; set; }
        public string name { get; set; }
        public string objects_uri { get; set; }
        public int? resource_type { get; set; }
        public string resource_uri { get; set; }
        public string verbose_name { get; set; }
        public string verbose_name_plural { get; set; }

        public ObjectModel()
        {
            fields = new List<ObjectFields>();
        }

        public static async void SaveWorkingObjectModel(ObjectModel workingObjectModel)
        {
            try
            {
                var workingObjectModelString = JsonConvert.SerializeObject(workingObjectModel);

                var workingGnericObjectFile = await App.localFolder.CreateFileAsync(workingObjectModelFileName, CreationCollisionOption.OpenIfExists);
                await FileIO.WriteTextAsync(workingGnericObjectFile, workingObjectModelString);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't save objectModel to file. Exception message: " + exception.Message);
            }
        }

        public static async Task<ObjectModel> GetWorkingObjectModel()
        {
            try
            {
                if (await App.FileExists(App.localFolder, workingObjectModelFileName))
                {
                    var workingObjectModelDataFile = await App.localFolder.CreateFileAsync(workingObjectModelFileName, CreationCollisionOption.OpenIfExists);
                    var workingObjectModelString = await FileIO.ReadTextAsync(workingObjectModelDataFile);

                    if (workingObjectModelString != String.Empty)
                    {
                        var workingObjectModel = JsonConvert.DeserializeObject<ObjectModel>(workingObjectModelString);
                        return workingObjectModel;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't get objectModel from file. Error: " + exception.Message);
                return null;
            }
        }

        public static async void DeleteWorkingObjectModel()
        {
            try
            {
                if (await App.FileExists(App.localFolder, workingObjectModelFileName))
                {
                    var file = await App.localFolder.GetFileAsync(workingObjectModelFileName);
                    await file.DeleteAsync();
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't delete workingObjectModelFile Error: " + exception.Message);
            }
        }
    }

    public class ObjectFields
    {
        public bool blank { get; set; }
        public string help_text { get; set; }
        public string name { get; set; }
        public bool Null {get; set;}
        public string objects_uri { get; set;}
        public string related_type { get; set; }
        public string table_uri { get; set; }
        public string resource_uri { get; set; }
        public string table { get; set; }
        public string type { get; set; }
        public string verbose_name { get; set; }
    }

    public class Track
    {
        public List<double[]> coordinates { get; set; }
        public string type { get; set; }

        public Track()
        {
            coordinates = new List<double[]>();
        }
    }
    
    public class Geometry
    {
        public double[] coordinates { get; set; }
        public string type { get; set; }
    }

    public class Crs
    {
        public CrsProperties properties { get; set; }
        public string type { get; set; }
    }

    public class CrsProperties
    {
        public string name { get; set; }
    }

    public class GenericVideo
    {
        public string file { get; set; }
        public int id { get; set; }
        public int object_id { get; set; }
        public string resource_uri { get; set; }
    }

    public class GenericImage : GenericVideo
    {
        public List<Thumbnails> thumbnails { get; set; }

        public GenericImage()
        {
            thumbnails = new List<Thumbnails>();
        }
    }

    public class Thumbnails
    {
        public string file { get; set; }
        public string name { get; set; }
    }
}
