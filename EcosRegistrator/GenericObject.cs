﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Windows.Web.Http.Filters;
using Windows.UI.Xaml.Controls;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    public class GenericObjectProperty : ObjectFields
    {
        public object propertyValue { get; set; }

        public GenericObjectProperty()
        { }

        public GenericObjectProperty(ObjectFields field)
        {
            this.blank = field.blank;
            this.help_text = field.help_text;
            this.name = field.name;
            this.Null = field.Null;
            this.resource_uri = field.resource_uri;
            this.table = field.table;
            this.type = field.type;
            this.verbose_name = field.verbose_name;
        }
    }


    public class GenericObjectTypedList : ObjectModel 
    {
        public List<GenericObject> listGenericObjects { get; set; }

        public GenericObjectTypedList()
        { 
            listGenericObjects = new List<GenericObject>();
        }

        public GenericObjectTypedList(List<GenericObject> listGenericObject, ObjectModel objectModel)
        { 
            this.listGenericObjects = listGenericObject;
            this.hidden = objectModel.hidden;
            this.icon = objectModel.icon;
            this.name = objectModel.name;
            this.objects_uri = objectModel.objects_uri;
            this.resource_type = objectModel.resource_type;
            this.resource_uri = objectModel.resource_uri;
            this.verbose_name = objectModel.verbose_name;
            this.verbose_name_plural = objectModel.verbose_name_plural;
            this.fields = objectModel.fields;
        }

        public GenericObjectTypedList(TypedObjectData typedObjectData)
        {
            this.listGenericObjects = GenericObject.MakeList(typedObjectData);
            this.hidden = typedObjectData.objectModel.hidden;
            this.icon = typedObjectData.objectModel.icon;
            this.name = typedObjectData.objectModel.name;
            this.objects_uri = typedObjectData.objectModel.objects_uri;
            this.resource_type = typedObjectData.objectModel.resource_type;
            this.resource_uri = typedObjectData.objectModel.resource_uri;
            this.verbose_name = typedObjectData.objectModel.verbose_name;
            this.verbose_name_plural = typedObjectData.objectModel.verbose_name_plural;
        }

        public static List<GenericObjectTypedList> GenerateList(List<TypedObjectData> typedObjectDatalist)
        {
            var typedGenericObjectList = new List<GenericObjectTypedList>();

            foreach (var typedObjectData in typedObjectDatalist)
            {
                var typedGenericobjects = new GenericObjectTypedList(typedObjectData);

                typedGenericObjectList.Add(typedGenericobjects);
            }

            return typedGenericObjectList;
        }
    }

    public class GenericObject
    {
        public Geometry geometry { get; set; }
        public Track track { get; set; }
        public List<GenericImage> images { get; set; }
        public List<GenericVideo> videos { get; set; }
        public string resourceUri { get; set; }
        public int id { get; set; }
        public string objectModelResourceUri { get; set; }
        public bool IsSended { get; set; }
        public Dictionary<string, GenericObjectProperty> properties{get; set;}

        const string workingGnericObjectFileName = "workingGnericObjecFile";
        
        public GenericObject()
        {
            geometry = new Geometry();
            track = new Track();
            images = new List<GenericImage>();
            videos = new List<GenericVideo>();
            resourceUri = String.Empty;
            objectModelResourceUri = String.Empty;
            IsSended = true;
            id = 0;
            properties = new Dictionary<string, GenericObjectProperty>();
        }

        public GenericObject(ObjectModel objectModel)
        {
            geometry = new Geometry();
            track = new Track();
            images = new List<GenericImage>();
            videos = new List<GenericVideo>();
            resourceUri = String.Empty;
            objectModelResourceUri = objectModel.resource_uri;
            IsSended = true;
            id = 0;
            properties = SetDictionary(objectModel);
        }

        static Dictionary<string, GenericObjectProperty> SetDictionary(ObjectModel objectModel)
        {
            var properties = new Dictionary<string, GenericObjectProperty>();

            foreach (var field in objectModel.fields)
            {
                if (field.name == App.geometryFieldName || field.name == App.trackFieldName || field.name == App.imagesFieldName || field.name == App.videosFieldName || field.name == App.resourseUriFieldName)
                    continue;

                properties.Add(field.name, new GenericObjectProperty(field));
            }

            return properties;
        }

        void SetDictionary(List<ObjectFields> fields, Dictionary<string, object> objectData)
        {
            properties = new Dictionary<string, GenericObjectProperty>();

            foreach (var field in fields)
            {
                if (field.name == App.geometryFieldName || field.name == App.trackFieldName || field.name == App.imagesFieldName || field.name == App.videosFieldName || field.name == App.resourseUriFieldName)
                    continue;

                object value = String.Empty;
                if (objectData.TryGetValue(field.name, out value))
                {
                    var genericObjectProperty = new GenericObjectProperty(field);
                    genericObjectProperty.propertyValue = value;

                    properties.Add(field.name, genericObjectProperty);
                }
            }
        }

        public static GenericObject GenerateObjectFromData(ObjectModel objectModel, Objectdata objectData)
        {
            var genericObject = new GenericObject();

            genericObject.geometry = objectData.geometry;
            genericObject.id = objectData.id;
            genericObject.objectModelResourceUri = objectModel.resource_uri;
            genericObject.IsSended = objectData.IsSended;
            
            //if (objectData[geometryFieldName] != null && objectData[geometryFieldName].ToString() != String.Empty)
            //    genericObject.geometry = JsonConvert.DeserializeObject<Geometry>(objectData[geometryFieldName].ToString());

            if (objectData.properties[App.trackFieldName] != null && objectData.properties[App.trackFieldName].ToString() != String.Empty)
                genericObject.track = JsonConvert.DeserializeObject<Track>(objectData.properties[App.trackFieldName].ToString());

            if (objectData.properties[App.imagesFieldName] != null && objectData.properties[App.imagesFieldName].ToString() != String.Empty)
                genericObject.images = JsonConvert.DeserializeObject<List<GenericImage>>(objectData.properties[App.imagesFieldName].ToString());

            if (objectData.properties[App.videosFieldName] != null && objectData.properties[App.videosFieldName].ToString() != String.Empty)
                genericObject.videos = JsonConvert.DeserializeObject<List<GenericVideo>>(objectData.properties[App.videosFieldName].ToString());

            if (objectData.properties[App.resourseUriFieldName] != null && objectData.properties[App.resourseUriFieldName].ToString() != String.Empty)
                genericObject.resourceUri = objectData.properties[App.resourseUriFieldName].ToString();

            //if (objectData[idFieldName] != null && objectData[idFieldName].ToString() != String.Empty)
            //    genericObject.id = Convert.ToInt32(objectData[idFieldName]);

            genericObject.SetDictionary(objectModel.fields, objectData.properties);

            return genericObject;
        }

        public static async Task<bool> SaveWorkingGenericObject(GenericObject workingGnericObject)
        {
            try
            {
                var workingGnericObjectString = JsonConvert.SerializeObject(workingGnericObject);

                var workingGnericObjectFile = await App.localFolder.CreateFileAsync(workingGnericObjectFileName, CreationCollisionOption.OpenIfExists);
                await FileIO.WriteTextAsync(workingGnericObjectFile, workingGnericObjectString);

                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't save genericObject to file. Exception message: " + exception.Message);
                return false;
            }
        }

        public static async Task<GenericObject> GetWorkingGenericObject()
        {
            try
            {
                if (await App.FileExists(App.localFolder, workingGnericObjectFileName))
                {
                    var workingGnericObjectDataFile = await App.localFolder.CreateFileAsync(workingGnericObjectFileName, CreationCollisionOption.OpenIfExists);
                    var workingGnericObjectString = await FileIO.ReadTextAsync(workingGnericObjectDataFile);

                    if (workingGnericObjectString != String.Empty)
                    {
                        var workingGnericObject = JsonConvert.DeserializeObject<GenericObject>(workingGnericObjectString);
                        return workingGnericObject;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(" Error: " + exception.Message);
                return null;
            }
        }

        public static async void DeleteWorkingGenericObject()
        {
            try
            {
                if (await App.FileExists(App.localFolder, workingGnericObjectFileName))
                {
                    var file = await App.localFolder.GetFileAsync(workingGnericObjectFileName);
                    await file.DeleteAsync();
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(" Error: " + exception.Message);
            }
        }
        
        public static List<GenericObject> MakeList(TypedObjectData typedObjectData)
        {
            try{
                if (typedObjectData.objectsData.features == null || typedObjectData.objectsData.features.Count == 0)
                    return null;
                
                var listGenericObject = new List<GenericObject>();

                foreach (var someObject in typedObjectData.objectsData.features)
                {
                    if (someObject == null)
                        continue;

                    var genericObject = GenericObject.GenerateObjectFromData(typedObjectData.objectModel, someObject);

                    listGenericObject.Add(genericObject);
                }

                return listGenericObject;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return null;
            }
        }

        public static string GenerateJsonString(GenericObject genericObject)
        {
            var postDictionary = new Dictionary<string, object>();

            if (genericObject.track != null && genericObject.track.coordinates != null && genericObject.track.coordinates.Count > 0)
                postDictionary.Add(App.trackFieldName, genericObject.track);
            else
                postDictionary.Add(App.trackFieldName, null);

            postDictionary.Add(App.geometryFieldName, genericObject.geometry);

            foreach (var property in genericObject.properties)
            {
                if (property.Key == null)
                    continue;

                if (property.Value == null)
                    continue;

                if (!property.Value.Null && (property.Value.propertyValue == null || property.Value.propertyValue.ToString() == String.Empty))
                    throw new Exception(String.Format("{0} обязательно для заполнения", property.Value.verbose_name));

                if (property.Value.type == FieldType.Integer)
                {
                    if (property.Value.propertyValue == null)
                        continue;

                    try
                    {
                        property.Value.propertyValue = Convert.ToInt32(property.Value.propertyValue);
                    }
                    catch (Exception exception)
                    {
                        throw new Exception(String.Format("{0} должно быть целым числом", property.Value.verbose_name));
                    }
                }

                if (property.Value.type == FieldType.Float)
                {
                    if (property.Value.propertyValue == null)
                        continue;

                    try
                    {
                        property.Value.propertyValue = Convert.ToDouble(property.Value.propertyValue);
                    }
                    catch (Exception exception)
                    {
                        throw new Exception(String.Format("{0} должно быть числом", property.Value.verbose_name));
                    }
                }

                if (property.Value.type == FieldType.DateTime)
                    if (property.Value.propertyValue != null)
                        postDictionary.Add(property.Value.name, Convert.ToDateTime(property.Value.propertyValue).ToString("yyyy-MM-ddTH:mm:ss"));
                    else
                        postDictionary.Add(property.Value.name, DateTime.Now.ToString("yyyy-MM-ddTH:mm:ss"));
                else
                    postDictionary.Add(property.Value.name, property.Value.propertyValue);
            }

            var jsonString = JsonConvert.SerializeObject(postDictionary);
            Debug.WriteLine(jsonString);

            return jsonString;
        }
    }
}
