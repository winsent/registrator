﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace EcosRegistrator
{
    class ApiEndpoints
    {
        public const string Authtorisation = "/login/";
        public const string GetToken = "/get-token/";
        public const string Logout = "/logout/";
        public const string AllObjectsScheme = "/userlayers/api/v1/tables/";
    }
}
