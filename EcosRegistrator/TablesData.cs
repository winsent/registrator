﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Windows.Web.Http.Filters;
using Windows.Storage;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
//needed becase IBuffer buffer.ToArray() here
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    class TablesData
    {
        const string tablesSchemeFileName = "TablesData.txt";

        public static ObjectsModel objectsScheme {get; set;}

        public async static Task<ObjectsModel> GetObjectsScheme()
        {
            try
            {
                if (await App.FileExists(App.localFolder, tablesSchemeFileName))
                {
                    var tablesShemeDataFile = await App.localFolder.CreateFileAsync(tablesSchemeFileName, CreationCollisionOption.OpenIfExists);
                    var tablesSchemeString = await FileIO.ReadTextAsync(tablesShemeDataFile);

                    if (tablesSchemeString != String.Empty)
                    {
                        objectsScheme = JsonConvert.DeserializeObject<ObjectsModel>(tablesSchemeString);
                        return objectsScheme;
                    }
                    else
                        return await GetTablesFromNet();
                }
                else
                    return await GetTablesFromNet();
            }
            catch (Exception exception)
            {
                Debug.WriteLine(" Error: " + exception.Message);
                return null;
            }
        }

        public async static void DeleteObjectsScheme()
        {
            try
            {
                if (await App.FileExists(App.localFolder, tablesSchemeFileName))
                {
                    var tablesShemeDataFile = await App.localFolder.GetFileAsync(tablesSchemeFileName);
                    await tablesShemeDataFile.DeleteAsync();

                    objectsScheme = null;
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(" Error: " + exception.Message);
            }
        }

        async static Task<ObjectsModel> GetTablesFromNet()
        {
            try
            {
                var currentEndpoint = new Uri(String.Format("{0}{1}", App.baseUrl, ApiEndpoints.AllObjectsScheme), UriKind.Absolute);
                var httpClient = new HttpClient();

                var response = await httpClient.GetAsync(currentEndpoint);

                if (response.StatusCode == HttpStatusCode.Ok)
                {
                    //decod from utf-8
                    var buffer = await response.Content.ReadAsBufferAsync();
                    var byteArray = buffer.ToArray();
                    var responseString = System.Text.Encoding.UTF8.GetString(byteArray, 0, byteArray.Length);

                    objectsScheme = JsonConvert.DeserializeObject<ObjectsModel>(responseString);
                    SaveTablesScheme(responseString);

                    return objectsScheme;
                }
                else
                {
                    Debug.WriteLine("Message: " + response.Content.ToString() + "; StatusCode: " + response.StatusCode.ToString());
                    throw new Exception("Message: " + response.Content.ToString() + "; StatusCode: " + response.StatusCode.ToString());
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine(" Error!!! " + exception.Message);
                return null;
            }
        }

        static async void SaveTablesScheme(string tablesSchemeString)
        {
            var tablesShemeDataFile = await App.localFolder.CreateFileAsync(tablesSchemeFileName, CreationCollisionOption.OpenIfExists);
            await FileIO.WriteTextAsync(tablesShemeDataFile, tablesSchemeString);
        }


    }
}
