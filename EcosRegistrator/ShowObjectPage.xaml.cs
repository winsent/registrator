﻿#region Namespaces
using EcosRegistrator.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Diagnostics;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.ApplicationModel.Activation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Geolocation;
using Windows.Web.Http;
#endregion

// Документацию по шаблону элемента "Основная страница" см. по адресу http://go.microsoft.com/fwlink/?LinkID=390556

namespace EcosRegistrator
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class ShowObjectPage : Page, IFileOpenPickerContinuable
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        FileOpenPicker FilePicker { get; set; }
        Progress<HttpProgress> Progress { get; set; }
        CancellationTokenSource CancellationSource { get; set; }
        GenericObject genericObject { get; set; }
        ObjectModel objectModel { get; set; }
        Geolocator geoLocation { get; set; }
        GridView GridViewImages { get; set; }
        GridView GridViewVideos { get; set; }

        public ShowObjectPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            CancellationSource = new CancellationTokenSource();
            Progress = new Progress<HttpProgress>();

            //MapHelper.SetMapToMyCurrentPosition(MapControl);
            GridViewImages = ListViewChildElements.CreateGridView("Photo");
            GridViewVideos = ListViewChildElements.CreateGridView("Video");
            SetPageEvents();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);

            genericObject = (GenericObject)e.Parameter;

            if (genericObject == null)
            {
                genericObject = await GenericObject.GetWorkingGenericObject();
            }

            if (genericObject == null)
            {
                return;
            }

            objectModel = TablesData.objectsScheme.objects.Find(model => model.resource_uri == genericObject.objectModelResourceUri);

            GenericObject.SaveWorkingGenericObject(genericObject);

            SetListViewControls();
        }

        void SetPageEvents()
        {
            AppBarButtonDeletePost.Click += async (sender, args) =>
            {
                StartProgressRing();
                DisableButtons();

                try
                {
                    if (genericObject == null || genericObject.resourceUri == null)
                        throw new Exception("genericObject == null or resurce uri == null");

                    var url = new Uri(String.Concat(App.baseUrl,genericObject.resourceUri), UriKind.Absolute);

                    if (await RequestsHelper.DeletePost(url))
                    {
                        Debug.WriteLine("DeletePost(url) --> status: succes.");

                        if (await TypedObjectData.DeleteSingleObject(genericObject))
                            Debug.WriteLine("TypedObjectData.DeleteSingleObject(genericObject) --> succes.");
                    }
                    else
                        throw new Exception("Не удается удалить объект.");

                    Frame.GoBack();
                }
                catch (Exception exception)
                {
#pragma warning disable
                    App.ShowMessageDialog("Произошла ошибка. " + exception.Message);
                    Debug.WriteLine(exception.Message);
                }

                StopProgressRing();
                EnableButtons();
            };

            AppBarButtonEditPost.Click += async (sender, args) =>
            {
                StartProgressRing();
                DisableButtons();

                try
                {
                    FillGenericObjectFields();

                    var jsonString = GenericObject.GenerateJsonString(genericObject);

                    if (await RequestsHelper.UpdateObject(jsonString, genericObject.resourceUri))
                    {
                        genericObject.IsSended = true;
                    }
                    else
                        genericObject.IsSended = false;

                    var objectData = new Objectdata(genericObject);

                    if (await TypedObjectData.UpdateSingleObject(objectData, objectModel))
                        App.ShowMessageDialog("Объект успешно обновлен!");
                }
                catch(Exception exception)
                {
                    Debug.WriteLine("Не удается отправить объект. Сообщение: {0}",exception.Message);
#pragma wqarning disable
                    App.ShowMessageDialog(exception.Message);
                }

                StopProgressRing();
                EnableButtons();
            };

            GridViewImages.ItemClick += (sender, args) =>
            {
                var grid = (Grid)args.ClickedItem;
                var genericImage = (GenericImage)grid.Tag;

                if (genericImage != null && genericImage.resource_uri != null && genericImage.resource_uri.ToString() != String.Empty)
                {
                    FillGenericObjectFields();
                    GenericObject.SaveWorkingGenericObject(genericObject);

                    Frame.Navigate(typeof(MediaViewerPage), genericImage);
                }
            };

            GridViewVideos.ItemClick += (sender, args) =>
            {
                var grid = (Grid)args.ClickedItem;
                var genericVideo = (GenericVideo)grid.Tag;

                if (genericVideo != null && genericVideo.file != null && genericVideo.file.ToString() != String.Empty)
                {
                    FillGenericObjectFields();
                    GenericObject.SaveWorkingGenericObject(genericObject);

                    Frame.Navigate(typeof(MediaViewerPage), genericVideo);
                }
            };

            //Goto TrackPage
            TextBlockTrack.Tapped += async(sender, args) =>
            {
                FillGenericObjectFields();
                GenericObject.SaveWorkingGenericObject(genericObject);

                Frame.Navigate(typeof(TrackPage), genericObject);
            };

            TextBlockDeleteTrack.Tapped += async (sender, args) =>
            {
                if (genericObject.track == null || genericObject.track.coordinates == null || genericObject.track.coordinates.Count == 0)
                {
                    FillGenericObjectFields();
                    GenericObject.SaveWorkingGenericObject(genericObject);

                    Frame.Navigate(typeof(TrackPage), genericObject);
                }
                else
                {
                    genericObject.track = null;
                }
            };

            AppBarButtonAddPhoto.Click += (sender, args) =>
            {
                Debug.WriteLine("Bar button photo clicked!");

                SetFilePickerTask(
                   PickerLocationId.PicturesLibrary,
                   new List<string>
                    {
                        ".bmp",
                        ".png",
                        ".jpeg",
                        ".jpg"
                    }
                );

                if (FilePicker != null)
                    FilePicker.PickSingleFileAndContinue();
            };

            AppBarButtonAddVideo.Click += (sender, args) =>
            {
                Debug.WriteLine("Bar button video clicked!");

                SetFilePickerTask(
                   PickerLocationId.VideosLibrary,
                   new List<string>
                    {
                        ".wmv",
                        ".mp4",
                        ".avi",
                        ".mov",
                        ".asf",
                        ".m2ts"
                    }
                );

                if (FilePicker != null)
                    FilePicker.PickSingleFileAndContinue();
            };

            Progress.ProgressChanged += (s, e) =>
            {
                double percentSent = 100;

                if (e.TotalBytesToSend > 0)
                    percentSent = e.BytesSent * 100 / e.TotalBytesToSend.Value;

                progressBar.Value = percentSent;

                Debug.WriteLine(percentSent);
            };
        }

        void SetListViewControls()
        {
            if (genericObject.geometry != null && genericObject.geometry.coordinates != null && genericObject.geometry.coordinates.Length != 0)
            {
                MapHelper.AddObjectPoint(MapControl, genericObject.geometry.coordinates);
                MapHelper.SetMapByCoordinates(MapControl, genericObject.geometry.coordinates);

                TextBoxLatitude.Text =  genericObject.geometry.coordinates[1].ToString();
                TextBoxLongitude.Text = genericObject.geometry.coordinates[0].ToString();
            }

            if (genericObject.track != null && genericObject.track.coordinates != null && genericObject.track.coordinates.Count > 0)
            {
                TextBlockDeleteTrack.Text = "\uE107";
                TextBlockTrack.Text = "Прикреплен трек";
            }

            foreach (var property in genericObject.properties.Values)
            { 
                switch (property.type)
                {
                    case FieldType.VarChar :
                        ListViewChildElements.CreateAndAddTextBoxFieldText(property, ListViewControls);
                        break;
                    case FieldType.Text:
                        ListViewChildElements.CreateAndAddTextBoxFieldText(property, ListViewControls, 120);
                        break;
                    case FieldType.DateTime:
                        ListViewChildElements.CreateDateTimeField(property, ListViewControls);
                        break;
                    case FieldType.Track:
                        Debug.WriteLine("genericObject have Track in properties!?...");
                        break;
                    case FieldType.Float:
                    case FieldType.Integer:
                        ListViewChildElements.CreateAndAddTextBoxFieldInteger(property, ListViewControls);
                        break;
                    case FieldType.Point:
                        Debug.WriteLine("generic have another 'field.type = 'point'' for saome reason");
                        break;
                    default:
                        break;
                }
            }

            if (genericObject.images != null && genericObject.images.Count > 0)
            {
                Debug.WriteLine("добавляем превьюшки");

                foreach (var image in genericObject.images)
                {
                    FillGridViewImages(image);
                }
            }

            if (genericObject.videos != null && genericObject.videos.Count > 0)
            {
                Debug.WriteLine("Добавляем видео");

                foreach (var video in genericObject.videos)
                { 
                    FillGridViewVideos(video);
                }
            }
        }

        void FillGridViewImages(GenericImage image)
        {
            if (GridViewImages.Items.Count == 0)
                ListViewControls.Items.Add(GridViewImages);

            Uri imageUri;
            var parentGrid = ListViewChildElements.CreateGridForGridView();
            parentGrid.Tag = image;

            if (image.thumbnails.Count > 0)
                imageUri = new Uri(String.Concat(App.baseUrl, image.thumbnails[0].file));
            else
                imageUri = new Uri(String.Concat(App.baseUrl, image.file));

            var imageWidth = ListViewChildElements.GetImageWidthForGridView();
            var imageView = ListViewChildElements.CreateImageView(imageUri, imageWidth);

            var buttonDeletePhoto = ListViewChildElements.CreateButtonDeletePhoto();

            buttonDeletePhoto.Click += async (sender, args) =>
            {
                Debug.WriteLine("You trying to delete photo. Url: " + String.Concat(App.baseUrl, image.resource_uri));

                DisableButtons();
                StartProgressRing();

                if (await RequestsHelper.DeleteFile(image.resource_uri))
                {
                    Debug.WriteLine("Photo successfully deleted!");
                    App.ShowMessageDialog("Фото удаленно. Объект обновлен.");

                    GridViewImages.Items.Remove(parentGrid);
                    genericObject.images.Remove(image);

                    var objectData = new Objectdata(genericObject);

                    if (await TypedObjectData.UpdateSingleObject(objectData, objectModel))
                        Debug.WriteLine("Объект успешно обновлен! url: {0}", genericObject.resourceUri);
                    else
                        Debug.WriteLine("Не получается обновить объект url: {0}", genericObject.resourceUri);
                }
                else
                {
                    await App.ShowMessageDialog("Не удается удалить фото.");
                }

                EnableButtons();
                StopProgressRing();
            };


            parentGrid.Children.Add(imageView);
            parentGrid.Children.Add(buttonDeletePhoto);
            GridViewImages.Items.Add(parentGrid);
        }

        void FillGridViewVideos(GenericVideo video)
        {
            if (GridViewVideos.Items.Count == 0)
                ListViewControls.Items.Add(GridViewVideos);

            var parentGrid = ListViewChildElements.CreateGridForGridView();
            parentGrid.Tag = video;

            var bitmapImage = new BitmapImage(new Uri("ms-appx:///Assets/videoIcon.png"));
            var imageView = ListViewChildElements.CreateImageViewFromBitmapImage(bitmapImage);

            var buttonDeleteVideo = ListViewChildElements.CreateButtonDeletePhoto();

            buttonDeleteVideo.Click += async (sender, args) =>
            {
                Debug.WriteLine("You trying to delete video. Url: " + String.Concat(App.baseUrl, video.resource_uri));

                DisableButtons();
                StartProgressRing();

                if (await RequestsHelper.DeleteFile(video.resource_uri))
                {
                    Debug.WriteLine("Video successfully deleted!");
                    App.ShowMessageDialog("Видео удаленно. Объект обновлен.");
                    
                    GridViewVideos.Items.Remove(parentGrid);
                    genericObject.videos.Remove(video);
                }
                else
                {
                    await App.ShowMessageDialog("Не удается удалить видео.");
                }

                EnableButtons();
                StopProgressRing();
            };

            parentGrid.Children.Add(imageView);
            parentGrid.Children.Add(buttonDeleteVideo);
            GridViewVideos.Items.Add(parentGrid);
        }

        void FillGenericObjectFields()
        {
            if (TextBoxLongitude.Text != null && TextBoxLatitude.Text != String.Empty && -180 < Convert.ToDouble(TextBoxLongitude.Text) &&
                Convert.ToDouble(TextBoxLongitude.Text) < 180 && Convert.ToDouble(TextBoxLatitude.Text) > -90 && Convert.ToDouble(TextBoxLatitude.Text) < 90)
            {
                genericObject.geometry.type = "Point";
                genericObject.geometry.coordinates = new double[] { Convert.ToDouble(TextBoxLongitude.Text), Convert.ToDouble(TextBoxLatitude.Text) };
            }
            else
            {
                genericObject.geometry.type = "Point";
                genericObject.geometry.coordinates = new double[] { 0, 0 };
            }

            foreach (var listViewItem in ListViewControls.Items)
            {
                var listViewItemTypeName = listViewItem.GetType().Name;
                var genericProperty = new GenericObjectProperty();

                switch (listViewItemTypeName)
                {
                    case "TextBox":
                        var textBox = (TextBox)listViewItem;

                        genericProperty = (GenericObjectProperty)textBox.Tag;

                        if (genericProperty.name == null)
                            continue;

                        if ((genericProperty.type == FieldType.Integer || genericProperty.type == FieldType.Float) && textBox.Text == String.Empty)
                            continue;

                        genericObject.properties[genericProperty.name].propertyValue = textBox.Text;

                        Debug.WriteLine(textBox.Text);
                        break;
                    case "Grid":
                        var grid = (Grid)listViewItem;

                        if (grid.Name == GridCoordinates.Name || grid.Name == GridTrack.Name)
                            continue;

                        var datePiker = grid.Children[1] as DatePicker;
                        genericProperty = (GenericObjectProperty)grid.Tag;

                        genericObject.properties[genericProperty.name].propertyValue = datePiker.Date.ToString();

                        Debug.WriteLine(datePiker.Date);
                        break;
                    default:
                        break;
                }

                Debug.WriteLine(listViewItemTypeName);
            }
        }

        void DisableButtons()
        {
            AppBarButtonDeletePost.IsEnabled = false;
            AppBarButtonEditPost.IsEnabled = false;
        }

        void EnableButtons()
        {
            AppBarButtonDeletePost.IsEnabled = true;
            AppBarButtonEditPost.IsEnabled = true;
        }

        void StartProgressRing()
        {
            progressRing.Visibility = Windows.UI.Xaml.Visibility.Visible;
            progressRing.IsActive = true;
            progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        void StopProgressRing()
        {
            progressRing.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            progressRing.IsActive = false;
            progressBar.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        void SetFilePickerTask(PickerLocationId locationId, List<string> listFileExtensions)
        {
            Debug.WriteLine("Setting FilePickerTask");

            FilePicker = new FileOpenPicker();
            FilePicker.SuggestedStartLocation = locationId;

            foreach (var item in listFileExtensions)
                FilePicker.FileTypeFilter.Add(item);
        }

        public async void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs args)
        {
            var file = args.Files.FirstOrDefault();

            if (file == null)
                return;

            StartProgressRing();
            DisableButtons();

            GenericObject updatedGeneric;
            ObjectFields field;

            switch (file.FileType)
            {
                case ".bmp":
                case ".png":
                case ".jpeg":
                case ".jpg":

                    field = objectModel.fields.FirstOrDefault(x => x.name == App.imagesFieldName);

                    if (field == null)
                        return;

                    if (await RequestsHelper.PostFile(file, genericObject.id.ToString(), field.objects_uri, Progress))
                    {
                        Debug.WriteLine("Photo successfully added");

                        var justPostetObject = await RequestsHelper.GetSingleObject(new Uri(String.Concat(App.baseUrl, genericObject.resourceUri), UriKind.Absolute));
                        await TypedObjectData.UpdateSingleObject(justPostetObject, objectModel);

                        updatedGeneric = GenericObject.GenerateObjectFromData(objectModel,justPostetObject);

                        if (updatedGeneric == null)
                            return;

                        foreach (var image in updatedGeneric.images)
                        {
                            if (genericObject.images.Exists(imageId => imageId.id == image.id))
                                continue;
                            else
                            {
                                FillGridViewImages(image);
                            }
                        }
                    }
                    else
                        await App.ShowMessageDialog("Не удалось добавить фото");

                    break;
                case ".wmv":
                case ".mp4":
                case ".avi":
                case ".mov":
                case ".asf":
                case ".m2ts":
                    field = objectModel.fields.FirstOrDefault(x => x.name == App.videosFieldName);

                    if (field == null)
                        return;

                    if (await RequestsHelper.PostFile(file, genericObject.id.ToString(), field.objects_uri, Progress))
                    {
                        Debug.WriteLine("Video successfully added");

                        var justPostetObject = await RequestsHelper.GetSingleObject(new Uri(String.Concat(App.baseUrl, genericObject.resourceUri), UriKind.Absolute));
                        await TypedObjectData.UpdateSingleObject(justPostetObject, objectModel);

                        updatedGeneric = GenericObject.GenerateObjectFromData(objectModel, justPostetObject);

                        foreach (var video in updatedGeneric.videos)
                        {
                            if (genericObject.images.Exists(videoId => videoId.id == video.id))
                                continue;
                            else
                            {
                                FillGridViewVideos(video);
                            }
                        }
                    }
                    else
                        await App.ShowMessageDialog("Не удалось добавить видео");

                    break;
                default:
                    break;
            }

            StopProgressRing();
            EnableButtons();
        }
        
        /// <summary>
        /// Получает объект <see cref="NavigationHelper"/>, связанный с данным объектом <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Получает модель представлений для данного объекта <see cref="Page"/>.
        /// Эту настройку можно изменить на модель строго типизированных представлений.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Заполняет страницу содержимым, передаваемым в процессе навигации.  Также предоставляется любое сохраненное состояние
        /// при повторном создании страницы из предыдущего сеанса.
        /// </summary>
        /// <param name="sender">
        /// Источник события; как правило, <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Данные события, предоставляющие параметр навигации, который передается
        /// <see cref="Frame.Navigate(Type, Object)"/> при первоначальном запросе этой страницы и
        /// словарь состояний, сохраненных этой страницей в ходе предыдущего
        /// сеанса.  Это состояние будет равно NULL при первом посещении страницы.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Сохраняет состояние, связанное с данной страницей, в случае приостановки приложения или
        /// удаления страницы из кэша навигации.  Значения должны соответствовать требованиям сериализации
        /// <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">Источник события; как правило, <see cref="NavigationHelper"/></param>
        /// <param name="e">Данные события, которые предоставляют пустой словарь для заполнения
        /// сериализуемым состоянием.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region Регистрация NavigationHelper

        /// <summary>
        /// Методы, предоставленные в этом разделе, используются исключительно для того, чтобы
        /// NavigationHelper для отклика на методы навигации страницы.
        /// <para>
        /// Логика страницы должна быть размещена в обработчиках событий для 
        /// <see cref="NavigationHelper.LoadState"/>
        /// и <see cref="NavigationHelper.SaveState"/>.
        /// Параметр навигации доступен в методе LoadState 
        /// в дополнение к состоянию страницы, сохраненному в ходе предыдущего сеанса.
        /// </para>
        /// </summary>
        /// <param name="e">Предоставляет данные для методов навигации и обработчики
        /// событий, которые не могут отменить запрос навигации.</param>

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
