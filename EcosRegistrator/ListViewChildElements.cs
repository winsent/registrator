﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Media;
using Windows.Storage;

namespace EcosRegistrator
{
    class FieldType
    {
        public const string VarChar = "varchar";
        public const string Text = "text";
        public const string Point = "point";
        public const string Track = "line_string";
        public const string DateTime = "datetime";
        public const string Integer = "integer";
        public const string Float = "float";
        public const string Related = "related";
    }

    class ListViewChildElements
    {
        public static double GetImageWidthForGridView()
        {
            var imageWidth = (Window.Current.Bounds.Width - 40) / 3;
            return imageWidth;
        }

        public static Grid CreateParentGrid(ListView listView, int height = 75)
        {
            var parentGrid = new Grid();
            parentGrid.Height = height;
            parentGrid.Width = listView.Width;

            //Thickness margin = parentGrid.Margin;
            //margin.Top += 10;
            //parentGrid.Margin = margin;
            parentGrid.Margin = new Thickness(5);

            return parentGrid;
        }

        public static Grid CreateGridForGridView(StorageFile file)
        {
            var parentGrid = new Grid();
            parentGrid.Tag = file;
            parentGrid.Margin = new Thickness(5);
            parentGrid.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(127, 200, 200, 200));

            return parentGrid;
        }

        public static Grid CreateGridForGridView()
        {
            var parentGrid = new Grid();
            parentGrid.Margin = new Thickness(5);
            parentGrid.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(127, 200, 200, 200));

            return parentGrid;
        }

        public static TextBlock CreateTitleTextBlock(string text, int marginLeft = 90, int fontSize = 26)
        {
            var textBlock = new TextBlock();
            textBlock.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            textBlock.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            textBlock.Height = 40;
            textBlock.FontSize = fontSize;

            Thickness margin = textBlock.Margin;
            margin.Top += 5;
            margin.Left += marginLeft;
            textBlock.Margin = margin;

            textBlock.Text = text;

            return textBlock;
        }

        public static Image CreateImageView(Uri sourceUrl, double squareSize = 60)
        {
            var imageView = new Image();
            imageView.Height = squareSize;
            imageView.Width = squareSize;
            imageView.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            imageView.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            imageView.Stretch = Windows.UI.Xaml.Media.Stretch.Fill;
            imageView.Margin = new Thickness(0, 5, 0, 5);

            var bitmapImage = new BitmapImage();
            var uri = sourceUrl;
            bitmapImage.UriSource = uri;
            imageView.Source = bitmapImage;

            return imageView;
        }

        public static Image CreateImageViewFromBitmapImage(BitmapImage bitmapImage)
        {
            var imageWidth = GetImageWidthForGridView();
            var imageView = new Image();
            imageView.Height = imageWidth;
            imageView.Width = imageWidth;
            imageView.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            imageView.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            imageView.Stretch = Windows.UI.Xaml.Media.Stretch.Fill;

            imageView.Source = bitmapImage;

            return imageView;
        }

        public static TextBlock CreateDatetimeTextBlock(DateTime datetime)
        {
            var textBlock = new TextBlock();
            textBlock.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Left;
            textBlock.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            textBlock.Height = 20;
            textBlock.Width = 170;
            textBlock.FontSize = 16;
            textBlock.Text = String.Format("создан: {0}", App.GetBuildedDateTimeString(datetime));


            Thickness margin = textBlock.Margin;
            margin.Top += 45;
            margin.Left += 90;
            textBlock.Margin = margin;

            return textBlock;
        }
        
        public static void CreateAndAddTextBoxFieldText(GenericObjectProperty property, ListView listView, int hieght = 45)
        {
            var textBox = TransparentTextBox(property);
            textBox.Height = hieght;
            listView.Items.Add(textBox);
        }

        public static void CreateAndAddTextBoxFieldInteger(GenericObjectProperty property, ListView listView)
        {
            var textBox = TransparentTextBox(property);
            
            InputScope scope = new InputScope();
            InputScopeName name = new InputScopeName();

            name.NameValue = InputScopeNameValue.Number;
            scope.Names.Add(name);
            textBox.InputScope = scope;

            listView.Items.Add(textBox);
        }

        public static void CreateDateTimeField(GenericObjectProperty property, ListView listView)
        {
            var parentGrid = CreateParentGrid(listView, 45);
            parentGrid.Tag = property;

            var textBlock = CreateTitleTextBlock(String.Concat(property.verbose_name, ":"), marginLeft: 10, fontSize: 22);
            textBlock.Width = 238;

            var dateTimePicker = new DatePicker();
            dateTimePicker.Margin = new Thickness(252, -5, 10, 0);

            if (property.propertyValue != null && property.propertyValue.ToString() != String.Empty)
                dateTimePicker.Date = Convert.ToDateTime(property.propertyValue);
            else
                dateTimePicker.Date = DateTime.Now;

            parentGrid.Children.Add(textBlock);
            parentGrid.Children.Add(dateTimePicker);
            listView.Items.Add(parentGrid);
        }

        static TextBox TransparentTextBox(GenericObjectProperty property)
        {
            var textBox = new TextBox();
            textBox.Tag = property;

            if (MainPage.textBoxTransparentStyle != null)
                textBox.Style = MainPage.textBoxTransparentStyle;

            if (!property.Null)
                textBox.BorderBrush = new SolidColorBrush(Color.FromArgb(255,255,170,170));
            
            textBox.PlaceholderText = property.verbose_name;

            if (property.propertyValue != null && property.propertyValue.ToString() != String.Empty)
                textBox.Text = property.propertyValue.ToString();

            return textBox;
        }

        public static GridView CreateGridView(string displayName)
        {
            var thickness = 5;
            var imageWidth = GetImageWidthForGridView();

            var gridView = new GridView();
            gridView.Name = "GridViewChiledElements";
            gridView.Margin = new Thickness(thickness);
            gridView.MinHeight = imageWidth + 2 * thickness;
            gridView.MaxHeight = 2 * gridView.MinHeight;
            gridView.Tag = displayName;
            gridView.IsItemClickEnabled = true;

            return gridView;
        }

        public static Button CreateButtonDeletePhoto()
        {
            var button = new Button();

            button.MinHeight = 40;
            button.MinWidth = 40;
            button.Height = 50;
            button.VerticalAlignment = VerticalAlignment.Top;
            button.HorizontalAlignment = HorizontalAlignment.Right;
            button.Background = new SolidColorBrush(Color.FromArgb(170,190,190,190));
            button.Margin = new Thickness(0,0,5,0);
            button.FontFamily = new FontFamily("Segoe UI Symbol");
            button.FontSize = 16;
            button.Content = "\uE107";

            return button;
        }
    }
}
