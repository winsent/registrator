﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.Storage;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.WindowsRuntime;

namespace EcosRegistrator
{
    class RegistratorServer
    {
        public string Name { get; set; }
        public string Adress { get; set; }

        public RegistratorServer()
        { }

        public RegistratorServer(string serverName, string serverAdress)
        {
            Name = serverName;
            Adress = serverAdress;
        }
    }

    class ServersData
    {
        const string serversFileName = "registratorServers.txt";

        static async Task<bool> SaveList(List<RegistratorServer> listServers)
        {
            try 
            {
                var serversJsonString = JsonConvert.SerializeObject(listServers);

                var dataFile = await App.localFolder.CreateFileAsync(serversFileName, CreationCollisionOption.OpenIfExists);
                await FileIO.WriteTextAsync(dataFile, serversJsonString);

                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't save ListDraftPosts. " + exception.Message);
                return false;
            }
        }

        public static async Task<bool> AddServer(RegistratorServer server)
        {
            try
            {
                var listServers = await GetList();

                if (listServers == null)
                    listServers = new List<RegistratorServer>();

                listServers.Add(server);

                return await SaveList(listServers);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't add server. " + exception.Message);
                return false;
            }
        }

        public static async Task<List<RegistratorServer>> GetList()
        {
            try 
            {
                var dataFile = await App.localFolder.CreateFileAsync(serversFileName, CreationCollisionOption.OpenIfExists);
                var serversDataString = await FileIO.ReadTextAsync(dataFile);

                if (serversDataString == String.Empty)
                    throw new Exception("file is empty");

                var listServers = JsonConvert.DeserializeObject<List<RegistratorServer>>(serversDataString);

                return listServers;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't get serversList. Mesage: " + exception.Message);

                return null;
            }
        }

        public static async Task<bool> DeleteServer(RegistratorServer server)
        {
            try
            {
                var listServers = await GetList();

                if (listServers == null)
                    throw new Exception("List servers is null.");

                var existedServer = listServers.Find(Server => Server.Name == server.Name);

                if (existedServer == null)
                    throw new Exception("Can't find given draft post in listDraftPosts");

                listServers.Remove(existedServer);
                Debug.WriteLine("draftPost successfully deleted!");

                return await SaveList(listServers);
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Cant't delete single DraftPost" + exception.Message);
                return false;
            }
        }

        public static async void ClearData()
        {
            try
            {
                if (await App.FileExists(App.localFolder, serversFileName))
                {
                    var dataFile = await App.localFolder.GetFileAsync(serversFileName);
                    await dataFile.DeleteAsync();
                }

                Debug.WriteLine("serversDataFile deleted or not exist!");
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Can't delete serversDataFile. " + exception.Message);
            }
        }
    }
}
